import React from "react";
import {observer} from "mobx-react";
import styles from "./login-page.module.scss";
import GoogleLogin from "react-google-login";


// @ts-ignore
export const LoginPageComponent: React.FC = observer(() => {
  const googleStyling = {
    border: "2px solid #2E2E2E",
    margin: "0 auto",
    borderRadius: "1em",
    backgroundColor: "white",
    height: `${styles.inputFieldHeight}`
  }
  return (
      <>
        <h3 className={styles.loginH3}>Soldier</h3>
        <form className={styles.loginForm}>
          <input className={styles.loginInputEmail} type="text"
                 name={"email_address"}
                 placeholder={"Email Address"}/>
          <input className={styles.loginInputPassword} type="text"
                 name={"password"} placeholder={"Password"}/>
          <p className={styles.loginDescriptionP}>We’ll sign you up if
            the email provided does not exist.</p>
          <a href={""} className={styles.submitButton}>Continue</a>
        </form>
        <div className={styles.loginBreakLineWrapper}>
          <div className={styles.loginBreakLineBefore}/>
          <p className={styles.loginBreakLine}>or sign in with</p>
          <div className={styles.loginBreakLineAfter}/>
        </div>

        <GoogleLogin
            clientId="658977310896-knrl3gka66fldh83dao2rhgbblmd4un9.apps.googleusercontent.com"
            buttonText="Login"
            cookiePolicy={'single_host_origin'}
            render={renderProps => (
                <button className='test'
                        style={googleStyling}>Google</button>
            )}
        />
      </>
  )
})

export const LoginPage: React.FC = observer(() => {
  return (
      <div className={styles.loginMainWrapper}>
        <LoginPageComponent/>
      </div>
  )
});

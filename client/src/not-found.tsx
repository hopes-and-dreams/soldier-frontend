import React from "react";

const notFoundStyles = {
  height: "50px"
}

export const NotFound: React.FC = () => {
  return (
      <div style={notFoundStyles}>
        <span>Oops, 404 Not Found</span>
      </div>
  )
}
import React, {useRef} from "react";
import styles from "./news.module.scss"
import {animated} from "react-spring"
import {RouteComponentProps, withRouter} from "react-router-dom";

const News: React.FC<RouteComponentProps> = ({history, match}) => {
  const index = useRef(0)
  const handleScroll = () => {
    console.log('scrolled!')
  }
  const onSwipe = () => {
    history.push("/news/8912aio");
  }

  return (
      <animated.h3
          className={`${styles.newsStyle}`}
          onClick={onSwipe}>News</animated.h3>
  )
}

export default withRouter(News);
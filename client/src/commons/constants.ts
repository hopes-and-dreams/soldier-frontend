import {authState} from "../store/is-login-store"

// Commons
export const REMOTE: string = "Remote";

// Enum
export const ALL_TASK: string = "All Tasks";
export const COMPLETED: string = "Completed";
export const ON_GOING: string = "On Going";
export const HIRING: string = "Hiring";

// Auth
export const NOT_AUTHORIZED: authState = "NOT_AUTHORIZED"
export const AUTHENTICATING: authState = "AUTHENTICATING"
export const AUTHORIZED: authState = "AUTHORIZED"

// Event
export const SCROLL_UP: string = "scroll_up"
export const SCROLL_DOWN: string = "scroll_down"

// Styling
export const MAX_HEADER_HEIGHT: number = 50;

// ClassNames
export const MAIN_COMPONENT_WRAPPER: string = "mainComponentWrapper";
export const ROOT_ID: string = "#root";
export const MISSION_COMPONENTS: string = "missionComponents";
export const PROFILE_COMPONENTS: string = "profileComponent";

// Url
export const NEWS_URL: string = "/news"
export const NEWS_URL_ID: string = "/news/:id"
export const MISSIONS_URL: string = "/missions"
export const MISSIONS_URL_ID: string = "/missions/:id"
export const PROFILES_URL: string = "/profiles"
export const PROFILES_URL_ID: string = "/profiles/:id"
export const PROFILES_URL_ID_REVIEWS: string = PROFILES_URL_ID + "/reviews"
export const LOGIN_URL: string = "/login"
export const ACCOUNT_SETTING_URL: string = "/account-settings"

// Colors
export const COLOR_PRI_BLACK: string = "#9a9a9a"
export const COLOR_SEC_BLACK: string = "#424242"
export const COLOR_COPIED_GREEN: string = "#4BB543"
export const COLOR_WHITE: string = "#FFFFFF"

// Icon Sizes
export const SIZE_CHAIN: string = "3.5vw"
export const SIZE_SHARE_ICON: string = "4vw"

// Spring configs
export const SPRING_CONFIG_HOR_SWIPE: object = {
  mass: 1,
  tension: 200,
  friction: 25,
}

// Spring object constants
export const BREAK_PHOTO_VIEW_DISTANCE: number = 200
import React, {useContext} from "react";
import {observer} from "mobx-react";
import styles from "./mission-view.module.scss";
import {mockedMissionProps} from "../missions/missions";
import {MissionViewerStore} from "../store/mission-viewer-store";
import {PhotoViewerStore} from "../store/photo-viewer-store";
import {ReactComponent as SoldierIcon} from "../icons/soldier_icon.svg";
import {TagComponent} from "../global/tag";
import {GetPriceString} from "../utils/utils";
import {ShareButtonComponent} from "../global/share";


// @ts-ignore
export const MissionViewerComponent: React.FC = observer(() => {
  const photoViewStore = useContext(PhotoViewerStore);
  const handleImgClick = () => {
    photoViewStore.updatePhotos(0, mockedMissionProps[0].missionInfo.photos)
    photoViewStore.updateOpenState(true)
  }
  const handleApplyClick = () => {
    console.log("Applied!")
    document.getElementById("root")!.classList.toggle("light");
    document.getElementById("root")!.classList.toggle("dark");
  }
  return (
      <>
        <h3 className={styles.missionViewTitleH3}>
          {mockedMissionProps[0].title}
        </h3>
        <p className={styles.missionViewInfoP}>{mockedMissionProps[0].missionInfo.description}</p>
        {mockedMissionProps[0].missionInfo.photos === null ? <div/> :
            <div className={styles.missionImgWrapper}>
              <div className={styles.missionImgWrapper}>
                <img className={styles.missionImg}
                     src={mockedMissionProps[0].missionInfo.photos![0].src}
                     alt={mockedMissionProps[0].missionInfo.photos![0].caption}
                     onClick={handleImgClick}/>
              </div>
            </div>}
        <div className={styles.missionTagShareWrapperDiv}>
          {TagComponent({
            text: mockedMissionProps[0].missionAddress
          })}
          {TagComponent({
            text: GetPriceString(mockedMissionProps[0].price)
          })}
          <ShareButtonComponent missionLink={"a link"}/>
        </div>
        <div className={styles.missionApplyWrapperDiv}>
          <button className={styles.missionApplyButton}
                  onClick={handleApplyClick}><SoldierIcon
              fill={'black'}/><span
              className={styles.missionApplySpan}>Apply</span>
          </button>
        </div>
      </>
  )
})

export const MissionViewer: React.FC = observer(() => {
  const missionViewStore = useContext(MissionViewerStore)
  if (missionViewStore.missionView.isOpen) {
    return (
        <div className={styles.missionViewClass}>
          <MissionViewerComponent/>
        </div>
    )
  } else {
    return <div/>
  }
});

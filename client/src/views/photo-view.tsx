import {animated, useSprings} from "react-spring"
import React, {useContext} from "react";
import {
  BREAK_PHOTO_VIEW_DISTANCE,
  SPRING_CONFIG_HOR_SWIPE
} from "../commons/constants";
import {useDrag} from "react-use-gesture";
import {clamp} from "lodash-es";
import {PhotoViewerStore} from "../store/photo-viewer-store";
import {observer} from "mobx-react";
import styles from "./photo-view.module.scss";

const cards = [
  'https://i.pinimg.com/originals/20/c4/ed/20c4ed904c96d955c7baed21e22d47e0.jpg',
  'https://upload.wikimedia.org/wikipedia/en/d/de/RWS_Tarot_01_Magician.jpg',
  'https://upload.wikimedia.org/wikipedia/en/thumb/8/88/RWS_Tarot_02_High_Priestess.jpg/690px-RWS_Tarot_02_High_Priestess.jpg'
]

const vw = Math.max(document.documentElement.clientWidth || 0)

export interface Photo {
  src: string
  caption: string
}

// @ts-ignore
export const PhotoViewer: React.FC = () => {
  const photoViewStore = useContext(PhotoViewerStore);
  const [images, setImages] = useSprings(photoViewStore.photoView.photos!.length, (i) => {
    return {
      x: (i - photoViewStore.photoView.currentIndex) * vw,
      y: 0,
      config: SPRING_CONFIG_HOR_SWIPE,
    }
  });

  const bind: Function = useDrag(({
                                    down,
                                    movement: [mx, my],
                                    direction: [xDir, yDir],
                                    distance,
                                    cancel,
                                  }) => {
    console.log("mx: " + mx + " my: " + my)
    if (down && distance > vw / 2 && cancel != null) {
      cancel()
      photoViewStore.updateCurrentIndex(clamp(photoViewStore.photoView.currentIndex + (xDir > 0 ? -1 : 1), 0, photoViewStore.photoView.photos!.length - 1));
    }
    if (Math.abs(yDir) >= 0.5 && cancel != null && my >= BREAK_PHOTO_VIEW_DISTANCE) {
      cancel()
      photoViewStore.updateOpenState(false)
    }
    setImages((i) => {
      const x = (i - photoViewStore.photoView.currentIndex) * vw + (down ? mx : 0)
      const y = down ? my : 0;
      return {x, y}
    })
  })

  return (
      images.map(({x, y}, i) => {
        return (
            <>
              <animated.figure  {...bind()} key={i} style={{x, y}}
                                className={styles.photoViewDiv}>
                <img src={photoViewStore.photoView.photos![i].src}
                     alt={photoViewStore.photoView.photos![i].caption}
                     className={styles.photoViewImg}/>
              </animated.figure>
              <figcaption
                  className={styles.photoViewCaption}>{photoViewStore.photoView.photos![photoViewStore.photoView.currentIndex].caption}</figcaption>
            </>
        )
      })
  )
}

export const PhotoViewerDiv: React.FC = observer(() => {
  const photoViewStore = useContext(PhotoViewerStore);
  console.log("PhotoViewerDiv", photoViewStore.photoView.photos)
  if (photoViewStore.photoView.isOpen) {
    return (
        <div className={styles.photoViewClass}>
          <PhotoViewer/>
        </div>
    )
  } else {
    return <div/>
  }
});

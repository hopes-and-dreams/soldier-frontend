import {MissionProps, Price} from "../missions/missions";
import {useEffect, useRef} from "react";

export const GetPriceString: (price: Price) => string = (price: Price) => {
  return `${price.currency} ${price.isRate ? price.amount + " / Hour" : price.amount}`
}

export const ConditionalRender: (condition: boolean, ifTrue: Object, ifFalse: Object) => Object = (condition: boolean, ifTrue: Object, ifFalse: Object) => {
  return condition ? ifTrue : ifFalse;
}

export const UsePrevious = (value: any) => {
  const ref = useRef<any>();

  useEffect(() => {
    ref.current = value;
  })
  return ref.current;
}

/**
 * @param missions
 * mission to be filtered
 * @param status
 * status of the mission
 * @param shouldFilter
 * will only filter if shouldFilter is true
 * @return {MissionProps}
 */
export const FilterMissions = (missions: MissionProps[], status: string, shouldFilter: boolean) => {
  return shouldFilter ? missions.filter(mission => mission.status === status) : missions;
}
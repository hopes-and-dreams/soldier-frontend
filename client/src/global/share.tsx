import React, {CSSProperties, useState} from "react";
import styles from "./share.module.scss";
import {IoIosShareAlt as ShareIcon} from 'react-icons/io'
import {AiOutlineLink as Chain} from 'react-icons/ai'
import * as clipboard from "clipboard-polyfill/text";
import {
  COLOR_COPIED_GREEN,
  COLOR_PRI_BLACK,
  COLOR_SEC_BLACK,
  COLOR_WHITE,
  SIZE_CHAIN
} from "../commons/constants";
import {ConditionalRender} from "../utils/utils";

interface Prop {
  missionLink: string
}

export const ShareButtonComponent: React.FC<Prop> = ({missionLink}) => {
  const [copied, setCopied] = useState(false);
  const [fill, setFill] = useState(COLOR_PRI_BLACK);
  const wrapperStyle = {
    backgroundColor: `${ConditionalRender(copied, COLOR_COPIED_GREEN, '')}`,
    border: `0.5vw ${ConditionalRender(copied, COLOR_WHITE, COLOR_PRI_BLACK)} solid`
  }
  const chainStyle: CSSProperties = {
    marginRight: "1vw",
    color: `${ConditionalRender(copied, COLOR_WHITE, COLOR_PRI_BLACK)}`
  }
  const textSpanStyle: CSSProperties = {
    fontSize: '3vw',
    color: `${ConditionalRender(copied, COLOR_WHITE, COLOR_PRI_BLACK)}`
  }
  const shareIconStyle: CSSProperties = {
    color: `${ConditionalRender(copied, COLOR_WHITE, COLOR_SEC_BLACK)}`,
    fontSize: '4vw'
  }

  const copyToClipboard = () => {
    clipboard.writeText(missionLink)
    document.execCommand('copy')
    setCopied(true)
  }
  return (
      <div className={styles.shareWrapperDiv} style={wrapperStyle}
           onClick={copyToClipboard}>
        <Chain size={SIZE_CHAIN} style={chainStyle}/>
        <span
            style={textSpanStyle}>{ConditionalRender(copied, 'Copied!', 'Share')}</span>
        <ShareIcon style={shareIconStyle}/>
      </div>
  )
}
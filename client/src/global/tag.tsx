import React from "react"
import styles from "./tag.module.scss"
import {MissionStatus} from "../missions/missions";

export interface TagProps {
  text?: string
  fontSize?: string
}

export const TagComponent: (tp: TagProps) => JSX.Element = (tp: TagProps) => {
  let tagClassName;
  if (tp.text === MissionStatus.COMPLETED) {
    tagClassName = styles.tagStatusCompleted
  } else if (tp.text === MissionStatus.HIRING) {
    tagClassName = styles.tagStatusHiring
  } else if (tp.text === MissionStatus.ON_GOING) {
    tagClassName = styles.tagStatusOnGoing
  } else {
    tagClassName = styles.tagNormal;
  }

  return <span className={`${tagClassName}`} style={{
    fontSize: tp.fontSize
  }}>{tp.text}</span>

}

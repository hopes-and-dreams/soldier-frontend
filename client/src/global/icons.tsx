import React, {CSSProperties} from "react";
import {ReactComponent as RankIconSvg} from "icons/rank1.svg";
import {ReactComponent as StarIconSvg} from "icons/rating-star.svg";
import styles from "./icons.module.scss"

interface IconStyle {
  width: string,
  wrapper?: {
    margin?: string,
    width?: string,
    position?: "relative" | "absolute" | "fixed",
  }
  height?: string,
  color?: string,
}

export const RankIcon: React.FC<IconStyle> = (style: IconStyle) => {
  const styling: CSSProperties = {
    margin: style.wrapper?.margin,
    width: style.wrapper?.width,
    position: style.wrapper?.position,
  }
  return (
      <div style={styling}>
        <RankIconSvg width={style.width}
                     height={style.height === null ? style.width : style.height}
                     stroke={styles.primaryColor}/>
      </div>
  )
}

export const StarRatingIcon: React.FC<IconStyle> = (style: IconStyle) => {
  const styling: CSSProperties = {
    margin: style.wrapper?.margin,
    width: style.wrapper?.width,
    position: style.wrapper?.position,
  }
  return (
      <div style={styling}>
        <StarIconSvg width={style.width}
                     height={style.height === null ? style.width : style.height}
                     fill={styles.primaryColor}/>
      </div>)
}
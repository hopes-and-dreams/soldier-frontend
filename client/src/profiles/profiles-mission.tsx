import React from "react";
import styles from "./userProfile.module.scss";
import {MissionProps} from "../missions/missions";
import {GetPriceString} from "../utils/utils";
import {TagComponent} from "../global/tag";
import {useHistory} from "react-router";
import {MISSIONS_URL_ID} from "../commons/constants";

interface ProfileMissionsProp {
  missions: MissionProps[]
}

export const ProfileMissions: React.FC<ProfileMissionsProp> = (missionProps) => {
  const history = useHistory();
  // TODO study about react router, links, and how it can affect
  //  states
  const handleProfileMissionsClick = () => {
    history.push(MISSIONS_URL_ID)
  }
  const missions = missionProps.missions.map((mission) => {
    return (
        <div className={styles.profileMissionsWrapper}
             onClick={handleProfileMissionsClick}>
          <div className={styles.profileMissionsStatusPriceWrapper}>
            {TagComponent({
              text: mission.status,
              fontSize: "2vw"
            })}
            {TagComponent({
              text: GetPriceString(mission.price),
              fontSize: "3vw"
            })}
          </div>
          <h4 className={styles.profileMissionsH4}>{mission.title}</h4>
          <p className={styles.profileMissionsP}>{mission.missionInfo.description}</p>
        </div>
    )
  })
  return (
      <div className={styles.profileMissionsMainWrapper}>
        {missions}
      </div>
  );
}
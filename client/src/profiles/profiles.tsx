import React, {useContext, useState} from "react";
import styles from "./userProfile.module.scss"
import {PROFILE_COMPONENTS} from "../commons/constants";
import {PhotoViewerStore} from "../store/photo-viewer-store";
import {withRouter} from "react-router";
import {Photo} from "../views/photo-view";
import {Reviews} from "./reviews";
import {RankIcon} from "../global/icons";
import {Gallery} from "./gallery";
import {mockedMissionProps} from "../missions/missions";
import {ProfileMissions} from "./profiles-mission";
import {FilterMissions} from "../utils/utils";

export interface Profile {
  profileImage: string
  name: string
  rank: string
}

export enum Filter {
  ALL_TASK = "All Tasks",
  COMPLETED = "Completed",
  HIRING = "Hiring",
  ON_GOING = "On Going",
}

const profileImage: Photo[] = [{
  src: "https://i.pinimg.com/originals/7a/07/75/7a07759bea9f1aee3daf38b1604d26ac.jpg",
  caption: "a fcking caption, hahaa"
}]

const galleryImages = [
  {
    src: "https://i.pinimg.com/originals/7a/07/75/7a07759bea9f1aee3daf38b1604d26ac.jpg",
    caption: "a caption"
  },
  {
    src: "https://i.pinimg.com/originals/0f/a6/22/0fa62255fa1735fd729319b4769246f7.jpg",
    caption: "a 94923490 i9fsdsdfaodjilakdjaldkja lsdkjd laskdj aldkzxj dldkj zldjxk djzxld "
  },
  {
    src: "https://i.pinimg.com/originals/0f/a6/22/0fa62255fa1735fd729319b4769246f7.jpg",
    caption: "a captionasld jad ja"
  },
  {
    src: "https://i.pinimg.com/originals/0a/37/2c/0a372cf3d6c27eab388ca1d075f6bc29.jpg",
    caption: "a caption asld 12"
  },
]

const MainUserProfile: React.FC = () => {
  const photoViewStore = useContext(PhotoViewerStore);
  let [missionsFilterStatus, setMissionsFilterStatus] = useState(Filter.ALL_TASK);
  const handleProfileImgClick = () => {
    photoViewStore.updatePhotos(0, profileImage)
    photoViewStore.updateOpenState(true)
  }
  const handleFilterClick = () => {
    // three states toggle switch
    if (missionsFilterStatus === Filter.ALL_TASK || missionsFilterStatus === undefined) {
      setMissionsFilterStatus(Filter.ON_GOING)
    } else if (missionsFilterStatus === Filter.ON_GOING) {
      setMissionsFilterStatus(Filter.HIRING)
    } else if (missionsFilterStatus === Filter.HIRING) {
      setMissionsFilterStatus(Filter.COMPLETED)
    } else if (missionsFilterStatus === Filter.COMPLETED) {
      setMissionsFilterStatus(Filter.ALL_TASK)
    }
  }

  return (
      <div id={PROFILE_COMPONENTS} className={styles.profileMainDiv}>
        {/* Upper info part*/}
        <div className={styles.profileUpperInfoPartWrapper}>
          <div className={styles.profileInfoWrapper}>
            <img src={profileImage[0].src}
                 alt={profileImage[0].caption}
                 className={styles.profileImage}
                 onClick={handleProfileImgClick}/>
            <div className={styles.profileInfo}>
              <div className={styles.profileNameRankWrapper}>
                <h3>{"Rachel Lai"}</h3>
                <RankIcon width={"100%"} wrapper={{
                  width: "5vw",
                  margin: "0 0 0 2vw"
                }}/>
              </div>
              <p>{"Professional Illustrator, Animator, and Logo Designer."}</p>
            </div>
          </div>
          <div className={styles.profileReviewButtonWrapper}>
            <button>{"Reviews"}</button>
            <span>{"herodotus94@gmail.com"}</span>
            <span>{`${"Langkawi"}, ${"Kedah"}`}</span>
          </div>
        </div>
        {/* Missions */}
        <div className={styles.titleFilterWrapper}>
          <h3>Missions</h3>
          <button
              onClick={handleFilterClick}>{missionsFilterStatus}</button>
        </div>
        <div className={styles.profileMissionsBackground}>
          <ProfileMissions
              missions={FilterMissions(mockedMissionProps, missionsFilterStatus, missionsFilterStatus !== Filter.ALL_TASK)}/>
        </div>
        {/* Gallery */}
        <h3 className={styles.profileGalleryH3}>Gallery</h3>
        <div className={styles.profileGalleryWrapper}>
          <Gallery photos={galleryImages}/>
        </div>
      </div>
  )
}

export default withRouter(MainUserProfile);
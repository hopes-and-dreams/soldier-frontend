import React, {useContext} from "react";
import {PhotoViewerStore} from "../store/photo-viewer-store";
import styles from "./userProfile.module.scss";
import {Photo} from "../views/photo-view";

interface GalleryProps {
  photos: Photo[]
}


export const Gallery: React.FC<GalleryProps> = ({photos}) => {
  const photoViewStore = useContext(PhotoViewerStore);
  const handleGalleryImgClick = (index: number) => {
    photoViewStore.updatePhotos(index, photos)
    photoViewStore.updateOpenState(true)
  }
  const galleryImagesDom = photos.map((photo, i) => {
    return (
        <div className={styles.profileGalleryImageWrapper}>
          <img src={photo.src} alt={photo.caption}
               className={styles.profileGalleryImage}
               onClick={() => handleGalleryImgClick(i)}/>
        </div>
    )
  })
  return (
      <div className={styles.profileGalleryImageMainWrapper}>
        {galleryImagesDom}
      </div>
  );
}
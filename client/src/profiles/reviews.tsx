// @ts-ignore
import React from "react";
import styles from "./reviews.module.scss";
import {Profile} from "./profiles";
import {observer} from "mobx-react";
import {RankIcon, StarRatingIcon} from "global/icons";
import {Link} from "react-router-dom";

export interface Reviews {
  reviewer: Profile
  rating: number
  message: string
}

const mockedReviews: Reviews[] = [{
  reviewer: {
    profileImage: "https://i.pinimg.com/originals/7a/07/75/7a07759bea9f1aee3daf38b1604d26ac.jpg",
    name: "Elizabeth John Wick",
    rank: "private",
  },
  rating: 4.9,
  message: "She is the best designer I know!!! 100% I would like to work with her again! She was really attentive and reply to my enquiries almost immediately"
}]

const ReviewsContentComponent: React.FC<Reviews> = ({reviewer, rating, message}) => {
  return (
      <div className={styles.profileReviewWrapper}>
        <div className={styles.profileReviewReviewerWrapper}>
          {/* Image */}
          <div className={styles.profileReviewImgWrapper}>
            <img
                src={"https://i.pinimg.com/originals/0f/a6/22/0fa62255fa1735fd729319b4769246f7.jpg"}
                className={styles.profileReviewImg}
                alt={"dont forget the alt"}/>
          </div>
          <div className={styles.profileReviewNameRatingWrapper}>
            <div className={styles.profileRevReviewerWrapper}>
              <h2>Elizabeth Jolin</h2>
              <RankIcon width={"100%"} wrapper={{
                margin: "0 0 0 2vw",
                width: "5vw",
                position: "relative"
              }}/>
            </div>
            {/* Star rating */}
            <div className={styles.profileReviewRatingWrapper}>
              <StarRatingIcon width={"100%"} wrapper={{
                margin: "0 1vw 0 0",
                width: "3vw",
                position: "relative"
              }}/>
              <span
                  className={styles.profileReviewRatingSpan}>4.8</span>
              <span className={styles.profileReviewTimeSpan}>23 October 2020</span>
              <Link to={"/missions/test"}
                    className={styles.profileReviewMission}>Help me
                design a logo for my
                nasi Lemak Stall </Link>
            </div>
          </div>
        </div>
        <p className={styles.profileReviewP}>She is the best designer
          I know!!! 100% I would like to work with her
          again! She was really attentive and reply to my enquiries
          almost immediately</p>
      </div>
  )
}

export const ReviewsComponent: React.FC = observer(() => {
  return (
      <div className={styles.reviewsContentWrapper}>
        {mockedReviews.map(rev => {
          return <ReviewsContentComponent reviewer={rev.reviewer}
                                          rating={rev.rating}
                                          message={rev.message}/>
        })}
      </div>
  )
})

export const ReviewsPage: React.FC = observer(() => {
  return (
      <div className={styles.reviewsMainWrapper}>
        <div className={styles.reviewsTitleWrapper}>
          <div className={styles.profileRatingIconHeaderWrapper}>
            <StarRatingIcon width={"100%"} wrapper={{
              width: "4vw",
              position: "relative",
              margin: "auto 1vw auto 0"
            }}/>
          </div>
          <h3 className={styles.reviewsTitle}>{`${4.66} (${1} Reviews)`}</h3>
        </div>
        <ReviewsComponent/>
      </div>
  )
});

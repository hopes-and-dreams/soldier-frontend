import {action, observable} from "mobx";
import {Photo} from "../views/photo-view";
import {createContext} from "react";
import {MissionProps, mockedMissionProps} from "../missions/missions";

interface MissionViewerProps extends MissionProps {
  isOpen: boolean
}

class missionViewerStore {
  @observable missionView: MissionViewerProps = mockedMissionProps[0] as MissionViewerProps

  @action updateMissionView = (photos: Photo[], title: string, description: string) => {
    this.missionView.title = title;
    this.missionView.missionInfo.photos = photos;
    this.missionView.missionInfo.description = description;
  }

  @action updateOpenState = (open: boolean) => {
    this.missionView.isOpen = open;
  }
}

export const MissionViewerStore = createContext(new missionViewerStore())
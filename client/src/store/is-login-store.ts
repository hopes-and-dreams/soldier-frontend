import {observable} from "mobx";
import {createContext} from "react";

export type authState =
    "NOT_AUTHORIZED"
    | "AUTHENTICATING"
    | "AUTHORIZED";

class isLogin {
  @observable loginState: authState;
}

export const IsLoginStore = createContext(new isLogin())
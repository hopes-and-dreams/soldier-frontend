import {action, observable} from "mobx";
import {createContext} from "react";
import {Photo} from "../views/photo-view";

interface PhotoViewerProps {
  isOpen: boolean
  photos?: Photo[]
  currentIndex: number
}


class photoViewerStore {
  @observable photoView: PhotoViewerProps = {
    isOpen: false,
    photos: [],
    currentIndex: 0,
  }

  @action updatePhotos = (index: number, photos?: Photo[]) => {
    this.photoView.photos = photos;
    this.photoView.currentIndex = index;
  }

  @action updateCurrentIndex = (index: number) => {
    this.photoView.currentIndex = index;
  }

  @action updateOpenState = (open: boolean) => {
    this.photoView.isOpen = open;
  }
}

export const PhotoViewerStore = createContext(new photoViewerStore())
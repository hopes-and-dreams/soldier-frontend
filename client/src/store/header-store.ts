import {action, observable} from "mobx";
import {createContext} from "react";
import {SCROLL_UP} from "../commons/constants";

interface headerScroller {
  xIndex: number // header pointer index
}

class headerStore {
  @observable xIndex: number = 0;
  @action updateXIndex = (i: number) => {
    this.xIndex = i;
  }
  @observable scrollState: string = SCROLL_UP
  @action setScrollState = (state: string) => {
    this.scrollState = state;
  }
}

export const HeaderStore = createContext(new headerStore())
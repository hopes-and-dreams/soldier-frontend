/**
 * Animation is pictured in physics, stiff means short animation time
 * **/
export const mediumSpring = {mass: 1, tension: 100, friction: 15}

export const stiffSpring = {mass: 1, tension: 500, friction: 25}
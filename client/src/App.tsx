import React from 'react';
import {
  BrowserRouter as Router,
  Route,
  Switch
} from 'react-router-dom';
import {Header} from "./headerComponent/header";
import {
  ACCOUNT_SETTING_URL,
  LOGIN_URL,
  MISSIONS_URL,
  MISSIONS_URL_ID,
  NEWS_URL,
  NEWS_URL_ID,
  PROFILES_URL,
  PROFILES_URL_ID,
  PROFILES_URL_ID_REVIEWS
} from "./commons/constants";
import {NotFound} from "./not-found";
import {PhotoViewerDiv} from "./views/photo-view";
import {LoginPage} from "./auth/login-page";
import {AccountSettings} from "./profiles/account-settings";
import {ReviewsPage} from "./profiles/reviews";
import {MissionViewer} from "./views/mission-view";
import {Home} from "./home";
import styles from "./home.module.scss";

function App() {

  return (
      <>
        <MissionViewer/>
        <PhotoViewerDiv/>
        <Header/>
        <AppWrapper/>
      </>
  );
}

const AppWrapper: React.FC = () => {
  return (
      <div className={styles.appWrapper}>
        <Router>
          <Switch>
            <Route exact
                   path={["/", NEWS_URL, NEWS_URL_ID, MISSIONS_URL, MISSIONS_URL_ID, PROFILES_URL, PROFILES_URL_ID]}
                   component={Home}/>
            <Route exact path={LOGIN_URL} component={LoginPage}/>
            <Route exact path={PROFILES_URL_ID_REVIEWS}
                   component={ReviewsPage}/>
            <Route exact path={MISSIONS_URL_ID}
                   component={MissionViewer}/>
            <Route exact path={ACCOUNT_SETTING_URL}
                   component={AccountSettings}/>
            <Route path="/" component={NotFound}/>
          </Switch>
        </Router>
      </div>
  )
}

export default App;


// localhost on mobile ifconfig | grep "inet " | grep -v 127.0.0.1

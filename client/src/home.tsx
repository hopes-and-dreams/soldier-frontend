// @ts-ignore
import React, {useContext} from "react";
import {withRouter} from "react-router-dom";
import {HeaderStore} from "./store/header-store";
import {
  MISSIONS_URL,
  MISSIONS_URL_ID,
  PROFILES_URL,
  PROFILES_URL_ID,
  SPRING_CONFIG_HOR_SWIPE
} from "./commons/constants";
import {animated, useSprings} from "react-spring";
import {useDrag} from "react-use-gesture";
import styles from "./home.module.scss"
import Missions from "./missions/missions";
import Profiles from "./profiles/profiles";
import {clamp} from "lodash-es";

export const fcArray: JSX.Element[] = [<Missions/>, <Profiles/>]

// @ts-ignore
export const Home: React.FC = withRouter(({history, match, location}) => {
  console.log("Home FC initialized!")
  const hStore = useContext(HeaderStore)
  switch (match.path) {
    case MISSIONS_URL:
    case MISSIONS_URL_ID:
      hStore.updateXIndex(0)
      break;
    case PROFILES_URL:
    case PROFILES_URL_ID:
      hStore.updateXIndex(1)
      break;
  }
  const wWidth = window.innerWidth;
  // const wWidth = 1;

  const [props, set] = useSprings(fcArray.length, (i) => {
    console.log("i: " + i)
    return {
      x: (i - hStore.xIndex) * wWidth,
      config: SPRING_CONFIG_HOR_SWIPE,
    }
  })

  const bind: Function = useDrag(({
                                    down,
                                    movement: [mx],
                                    direction: [xDir, yDir],
                                    distance,
                                    cancel,
                                  }) => {
    if (down && distance > wWidth / 2 && cancel != null) {
      cancel()
      hStore.updateXIndex(clamp(hStore.xIndex + (xDir > 0 ? -1 : 1), 0, fcArray.length - 1));
    }
    console.log("clicked!")
    if (Math.abs(yDir) >= 2 && cancel != null) {
      cancel()
    }
    set((i) => {
      const x = (i - hStore.xIndex) * wWidth + (down ? mx : 0)
      return {x}
    })

  })

  const mainComponentList = props.map(({x}, i) => {
    return (
        <animated.div {...bind()} key={i} style={{x}}
                      className={styles.componentWrapper}>
          {fcArray[i]}
        </animated.div>
    )
  })

  return (
      <div className={styles.mainComponentWrapper}>
        {mainComponentList}
      </div>
  )
})
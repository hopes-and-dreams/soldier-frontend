import React, {useContext, useEffect, useRef} from "react";
import styles from "./header.module.scss";
import {
  MISSION_COMPONENTS,
  PROFILE_COMPONENTS,
  ROOT_ID,
  SCROLL_DOWN,
  SCROLL_UP
} from "../commons/constants";
import {HeaderStore} from "../store/header-store";
import {animated, useSpring} from "react-spring";
import {observer} from "mobx-react";
import {fcArray} from "../home";

const rootDiv = document.documentElement.querySelector(ROOT_ID) as HTMLDivElement;
let missionComponent: HTMLDivElement;
let profileComponent: HTMLDivElement;
let scrollReady: number = 0;
const vh = Math.max(document.documentElement.clientHeight || 0)
// const vh = document.documentElement.clientHeight;
const HeaderPointer = observer(() => {
  const hStore = useContext(HeaderStore);
  const headerSpring = useSpring(
      {x: (hStore.xIndex * (rootDiv.offsetWidth / fcArray.length))});
  return (
      <animated.div className={styles.headerPointer}
                    style={headerSpring}/>
  );
})

export const Header: React.FC = () => {
  console.log("header")
  const headerHeight = vh * 0.09; // headerHeight - headerPointerHeight - deltaHeight
  let [scroll] = React.useState(0)
  const hStore = useContext(HeaderStore)
  const [scrollState, setScrollState] = React.useState(hStore.scrollState)
  const refHeader: React.MutableRefObject<any> = useRef<HTMLDivElement>(null)
  const styleSelector: () => React.CSSProperties | undefined = () => {
    if (scrollState === SCROLL_UP) {
      rootDiv.style.marginTop = '10vh'
      return styleTop
    } else if (scrollState === SCROLL_DOWN) {
      rootDiv.style.marginTop = '0vh'
      return styleBottom
    }
  }
  // use time transition and 2 states

  const styleTop: React.CSSProperties = {
    top: `0px`,
    backgroundColor: "white",
  }

  const styleBottom: React.CSSProperties = {
    top: `-${headerHeight}px`,
    backgroundColor: "lawngreen",
  }

  const handleComponentScroll = (scrollTop: number) => {
    console.log("scroll: " + scroll + " scrollTop: " + scrollTop + " scrollReady: " + scrollReady)
    if (scrollReady >= 2) {
      console.log("handleScrollMissionComponent")
      // down is negative, up is positive
      if (scroll > scrollTop) {
        // scroll up
        console.log("set scroll up")
        hStore.setScrollState(SCROLL_UP)
        setScrollState(SCROLL_UP) // This caused state to rerender
        scrollReady = 0;
      } else {
        // scroll down
        console.log("set scroll down")
        hStore.setScrollState(SCROLL_DOWN)
        setScrollState(SCROLL_DOWN)
        scrollReady = 0;
      }
    }
    scrollReady += 1;
    scroll = scrollTop;
  };

  useEffect(() => {
    missionComponent = document.getElementById(MISSION_COMPONENTS) as HTMLDivElement;
    profileComponent = document.getElementById(PROFILE_COMPONENTS) as HTMLDivElement;
    missionComponent.addEventListener('scroll', () => handleComponentScroll(missionComponent.scrollTop));
    profileComponent.addEventListener('scroll', () => handleComponentScroll(profileComponent.scrollTop));
    // rootDiv.addEventListener('scroll', handleScroll);

    // this gets called when component is unmounted
    return () => {
      missionComponent.removeEventListener('scroll', () => handleComponentScroll(missionComponent.scrollTop))
      profileComponent.removeEventListener('scroll', () => handleComponentScroll(profileComponent.scrollTop))
    }
  }, [])


  return (
      <div className={`${styles.headerStyle}`}
           style={styleSelector()}
           ref={refHeader}>
        <div className={styles.headerH3Wrapper}>
          <h3>Missions</h3>
          <h3>Profile</h3>
        </div>
        <HeaderPointer/>
      </div>
  );
}
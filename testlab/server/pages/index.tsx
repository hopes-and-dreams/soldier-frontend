import Head from 'next/head'
import styles from '../styles/Home.module.css'
import React, {useContext, useEffect, useState} from "react";
import {SadStore} from "../store/sad-store";
import {observer} from "mobx-react";

// @ts-ignore
const SadComponent = observer(({s}) => {
  const sadStore = useContext(SadStore);

  return <span>Sad: {sadStore.sad.index} </span>
})

let i = 0;
let arr:number[] = [];
for (i = 0; i < 10; i++) {
  arr.push(i)
}

const HomeDiv = () => {
  const [s1, setS1] = useState(0)
  const sadStore = useContext(SadStore);
  const handleCs = () => {
    console.log("handleCs", sadStore.sad.index)
    setS1(sadStore.sad.index + 1)
    sadStore.updateCurrentIndex(sadStore.sad.index + 1)
  }

  return (
    <div className={styles.container}>
      <span>{s1}</span>
      {arr.map((i) => {
        console.log("rederned" + i)
        return <span>{i}</span>
      })}
      <button onClick={handleCs}>Change State</button>
    </div>
  )
}

export default observer(HomeDiv);
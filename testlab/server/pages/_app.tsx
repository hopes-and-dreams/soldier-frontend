import '../styles/globals.css'
import {AppProps} from "next/app";
import React from "react";
import {
  observer,
} from "mobx-react";

function MyApp({ Component, pageProps }: AppProps) {
  return <Component {...pageProps} />
}

export default observer(MyApp)

import {action, observable} from "mobx";
import {createContext} from "react";
import {enableStaticRendering} from "mobx-react-lite";

interface PhotoViewerProps {
  index: number
}

class sadStore {
  sad: PhotoViewerProps = {
    index: 0,
  }

  updateCurrentIndex = (index: number) => {
    this.sad.index = index;
  }
}

export const SadStore = createContext(new sadStore())
import React, {
  createContext,
  FormEvent,
  useContext,
  useState
} from "react";
import {observer} from "mobx-react";
import styles from "./sign-up-page.module.scss";
import {Api} from "../../api/api";


// @ts-ignore
export const SignUpPageComponent: React.FC = observer(() => {
  const api = useContext(createContext(Api))
  const [email, setEmail] = useState<string>()
  const [pin, setPin] = useState<string>()
  const handleSubmit = (e: FormEvent<EventTarget>) => {
    e.preventDefault()
    const res = api.postRest("/sign-up", undefined, {
      email: email,
      pin: pin
    })
    res.then(res => {
      console.log(res.data)
      console.log(res.headers)
    })
  }
  const handleEmailChange = (value: string) => {
    setEmail(value)
  }
  const handlePinChange = (value: string) => {
    setPin(value)
  }

  return (
      <>
        <h3 className={styles.signUpH3}>Soldier</h3>
        <form className={styles.signUpForm}
              onSubmit={(e) => handleSubmit(e)}>
          <input className={styles.signUpInputEmail} type="text"
                 name={"email_address"}
                 placeholder={"Email Address"}
                 onChange={e => handleEmailChange(e.target.value)}/>
          <input className={styles.signUpInputEmail} type="text"
                 name={"pin"}
                 placeholder={"Pin"}
                 onChange={(e) => handlePinChange(e.target.value)}/>
          <p className={styles.signUpDescriptionP}>We’ll sign you up
            if
            the email provided does not exist.</p>
          <button type={"submit"} value={"Submit"}
                  className={styles.submitButton}>Continue
          </button>
        </form>

        <div className={styles.signUpBreakLineWrapper}>
          <div className={styles.signUpBreakLineBefore}/>
          <p className={styles.signUpBreakLine}>or sign in with</p>
          <div className={styles.signUpBreakLineAfter}/>
        </div>
      </>
  )
})

const SignUpPage: React.FC = observer(() => {
  return (
      <div className={styles.signUpMainWrapper}>
        <SignUpPageComponent/>
      </div>
  )
});

export default SignUpPage;
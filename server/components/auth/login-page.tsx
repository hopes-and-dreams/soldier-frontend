import React, {
  createContext,
  useContext,
  useEffect,
  useState
} from "react";
import {observer} from "mobx-react";
import styles from "./login-page.module.scss";
import {Api} from "../../api/api";
import {useRouter} from "next/router";


// @ts-ignore
export const LoginPageComponent: React.FC = observer(() => {
  const api = useContext(createContext(Api))
  const [email, setEmail] = useState<string>()
  const [pin, setPin] = useState<string>()
  const router = useRouter();
  const handleEmailChange = (value: string) => {
    setEmail(value)
  }
  const handlePinChange = (value: string) => {
    setPin(value)
  }

  useEffect(() => {
    router.push('/profile/edit')
  })

  return (
      <>
        <h3 className={styles.loginH3}>Login</h3>
      </>
  )
})

const LoginPage: React.FC = observer(() => {
  return (
      <div className={styles.log}>
        <LoginPageComponent/>
      </div>
  )
});

export default LoginPage;
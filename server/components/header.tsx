import React, {useContext, useEffect, useRef} from "react";
import styles from "styles/components/header.module.scss";
import {
  CHAT_VIEW_URL_ID,
  MISSION_COMPONENTS,
  MISSIONS_URL,
  PROFILES_URL,
  SCROLL_DOWN,
  SCROLL_UP
} from "../commons/constants";
import {HeaderStore} from "../store/header-store";
import {mockedProfile} from "./profiles/profile";
import {GetCurrentMainComponent} from "../utils";
import {ChatIcon} from "./global/icons";
import {ShareButtonComponent} from "./global/share";
import {BackButton} from "./global/back";
import {useRouter} from "next/router";
import ProfilePicture from "./profiles/ProfilePicture";
import HireButton from "./chat/hire";
import Link from "next/link";
import {v4 as uuidV4} from "uuid"

let mainComponent: HTMLDivElement;
let scrollReady: number = 0;
const vh: string = "5vh";

interface headerProps {
  width?: number
  index?: number
}

const getActionButton = (pathName: string, linkToShare: string) => {
  const userId = uuidV4()
  switch (pathName) {
    case PROFILES_URL:
      return <Link href={`/chat/${userId}`}><a><ChatIcon
          width={"3.5em"} color={"black"}/></a></Link>
    case MISSIONS_URL:
      return <ShareButtonComponent linkToShare={linkToShare}/>
    case CHAT_VIEW_URL_ID:
      return <HireButton/>
  }
  return <div/>
}

export const Header: React.FC<headerProps> = () => {
  let [scroll] = React.useState(0)
  const hStore = useContext(HeaderStore)
  const router = useRouter()
  const [scrollState, setScrollState] = React.useState(hStore.scrollState)
  const refHeader: React.MutableRefObject<any> = useRef<HTMLDivElement>(null)

  const handleComponentScroll = (scrollTop: number) => {
    // console.log("scroll: " + scroll + " scrollTop: " + scrollTop + " scrollReady: " + scrollReady)
    if (scrollReady >= 2) {
      console.log("handleScrollMissionComponent")
      // down is negative, up is positive
      if (scroll > scrollTop) {
        // scroll up
        console.log("set scroll up")
        setScrollState(SCROLL_UP)
        refHeader.current.style.height = vh
        refHeader.current.style.top = 0
        scrollReady = 0;
      } else {
        // scroll down
        console.log("set scroll down")
        setScrollState(SCROLL_DOWN)
        refHeader.current.style.height = 0
        refHeader.current.style.top = "-" + vh
        scrollReady = 0;
      }
    }
    scrollReady += 1;
    scroll = scrollTop;
  };
  useEffect(() => {
    mainComponent = document.getElementById(GetCurrentMainComponent(router.pathname)) as HTMLDivElement;
    mainComponent.addEventListener('scroll', () => handleComponentScroll(mainComponent.scrollTop));
    // this gets called when component is unmounted
    return () => {
      mainComponent.removeEventListener('scroll', () => handleComponentScroll(mainComponent.scrollTop))
    }
  }, [])

  return (
      <div className={`${styles.headerStyle} ${scrollState}`}
           ref={refHeader}>
        <div className={styles.headerProfileDiv}>
          {GetCurrentMainComponent(router.pathname) === MISSION_COMPONENTS ?
              <ProfilePicture
                  caption={mockedProfile.profileImage[0].caption}
                  src={mockedProfile.profileImage[0].src}
                  width={"5em"}
                  link={PROFILES_URL}/> : <BackButton/>}
          <div className={styles.headerNameWrapper}>
            <h3>{mockedProfile.name}</h3>
          </div>
          {getActionButton(router.pathname, "linkHere")}
        </div>
      </div>
  );
}

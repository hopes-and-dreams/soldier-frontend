import {animated, useSprings} from "react-spring"
import React, {useEffect, useState} from "react";
import {
  BREAK_PHOTO_VIEW_DISTANCE,
  SPR_CONF_RESPONSIVE
} from "commons/constants";
import {useDrag} from "react-use-gesture";
import styles from "../../styles/views/photo-view.module.scss";
import {Clamp} from "../../utils";
import {observer} from "mobx-react";


let vw: number;

export interface Photo {
  src: string
  caption: string
}

interface PhotoViewerProps {
  photos?: Photo[]
  currentIndex: number
  show?: boolean
  handleImageClickView: (isOpen: boolean, index: number) => void
}


// @ts-ignore
export const PhotoViewerComponent: React.FC<PhotoViewerProps> = observer((props: PhotoViewerProps) => {
  const [index, setIndex] = useState(props.currentIndex);
  const [images, setImages] = useSprings(props.photos!.length, (i) => {
    return {
      x: (i - props.currentIndex) * vw,
      y: 0,
      config: SPR_CONF_RESPONSIVE,
    }
  });

  const bind: Function = useDrag(({
                                    down,
                                    movement: [mx, my],
                                    direction: [xDir, yDir],
                                    distance,
                                    cancel,
                                    canceled
                                  }) => {
    console.log("dragged")
    if (down && Math.abs(mx) > vw / 2 && cancel != null) {
      console.log("canccel")
      cancel()
      setIndex(Clamp(index + (xDir > 0 ? -1 : 1), 0, props.photos!.length - 1));
    }
    const myAbs = Math.abs(my)
    if (myAbs >= vw / 2 && cancel != null || myAbs >= BREAK_PHOTO_VIEW_DISTANCE) {
      cancel()
      props.handleImageClickView(false, 0)
    }
    if (canceled) {
      console.log("got canceled")
    }
    // @ts-ignore
    setImages((i) => {
      const x = (i - index) * vw + (down ? mx : 0)
      const y = down ? my : 0;
      return {x, y}
    })
  })


  useEffect(() => {
    vw = Math.max(document.documentElement.clientWidth || 0)
    setImages((i) => {
      const x = (i - index) * vw
      return {x}
    })
  }, [])

  return (
      images.map(({x, y}, i) => {
        return (
            <>
              <animated.figure  {...bind()} key={i} style={{x, y}}
                                className={styles.photoViewDiv}>
                <img src={props.photos![i].src}
                     alt={props.photos![i].caption}
                     className={styles.photoViewImg}/>
                <figcaption
                    className={styles.photoViewCaption}>
                  <span>{props.photos![i].caption}</span></figcaption>
              </animated.figure>
              <div className={styles.photoViewCaptionBackground}/>
            </>
        )
      })
  )
})

export const PhotoViewer: React.FC<PhotoViewerProps> = observer((props: PhotoViewerProps) => {
  if (!Array.isArray(props.photos) || !props.photos.length || !props.show) {
    return <div/>
  } else {
    return (
        <div className={styles.photoViewClass}>
          <PhotoViewerComponent {...props}/>
        </div>
    )
  }
});

import React, {useEffect} from "react";
import styles from "styles/components/photo-add.module.scss";
import {animated, useSpring} from "react-spring";
import {useDrag} from "react-use-gesture";
import {
  ID_CAPTION,
  SPR_CONF_RESPONSIVE
} from "../../commons/constants";
import {Photo} from "./photo-view";

const cards = [
  'https://i.pinimg.com/originals/20/c4/ed/20c4ed904c96d955c7baed21e22d47e0.jpg',
  'https://upload.wikimedia.org/wikipedia/en/d/de/RWS_Tarot_01_Magician.jpg',
  'https://upload.wikimedia.org/wikipedia/en/thumb/8/88/RWS_Tarot_02_High_Priestess.jpg/690px-RWS_Tarot_02_High_Priestess.jpg'
]

interface photoAddProps {
  url: string
  handleSetShowHide: (showHide: boolean) => void
  handleSetPhotos: (photo: Photo) => void
}

let vw: number;
export const PhotoAddView: React.FC<photoAddProps> = (props: photoAddProps) => {
  const [{x, y, opacity}, setImage] = useSpring(() => ({
    x: 0,
    y: 0,
    opacity: 1,
    config: SPR_CONF_RESPONSIVE
  }))
  const handleImgAdd = () => {
    console.log("Img Added")
    const caption = document.getElementById(ID_CAPTION) as HTMLInputElement;
    props.handleSetPhotos({
      src: props.url,
      caption: caption.value
    })
    props.handleSetShowHide(false)
  }
  const bind: Function = useDrag(({
                                    down,
                                    movement: [mx, my],
                                    distance,
                                    cancel,
                                  }) => {
    const exit = vw / 2
    if (down && distance > exit && cancel != null) {
      props.handleSetShowHide(false)
      cancel()
      console.log("ccancel!")
    }
    // @ts-ignore
    setImage(() => {
      const x = down ? mx : 0;
      const y = down ? my : 0;
      // make number range between 0 to 1
      let opacity = (exit - distance) / exit;
      if (!down) {
        opacity = 1
      }
      return {x, y, opacity}
    })
  })

  useEffect(() => {
    vw = Math.max(document.documentElement.clientWidth || 0)
  }, [])

  return (
      <div className={styles.photoAddWrapper}>
        <animated.div {...bind()} style={{x, y, opacity}}
                      className={styles.photoAddImgWrapper}>
          <img src={props.url} // no need alt since just temporary
               className={styles.photoAddImg}/>
        </animated.div>
        <div className={styles.photoAddCaptionSpan}>
          <span>Caption</span>
          <button onClick={handleImgAdd}>Send</button>
        </div>
        <textarea name={ID_CAPTION}
                  id={ID_CAPTION}
                  className={styles.photoAddCaption}
                  placeholder={"Add a caption or not..."}
                  spellCheck={false}/>
      </div>
  )
}


import React, {useState} from "react";
import styles from "styles/components/chat.module.scss"
import {HIRE_STATE, HIRED_STATE} from "../../commons/constants";

const HireButton: React.FC = () => {
  const [hireState, setHireState] = useState(HIRE_STATE);

  const handleHireButton = () => {
    console.log("HIRE!")
    setHireState(HIRED_STATE)
  }
  return (
      <button className={styles.hireButton}
              onClick={handleHireButton}>
        {hireState}
      </button>
  )
}

export default HireButton;
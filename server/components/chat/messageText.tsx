import React from "react";
import styles from "styles/components/chat.module.scss";
import {TimeNow} from "../global/time";
import moment from "moment";
import {ONE_DAY, TWO_MINUTES} from "../../commons/constants";

export interface MessageTextProps {
  userName: string
  data: string
  timestamp: number
}

interface MessageProps {
  message: MessageTextProps
  prevMessage: MessageTextProps
  first: boolean
}

export const mockedTextMessages: MessageTextProps[] = [{
  userName: "Siang",
  data: "Hello Alice",
  timestamp: 1610454723 - ONE_DAY - 200
}, {
  userName: "Alice",
  data: "Hi, so you are the one who wants to get your room cleaned?",
  timestamp: 1610454723 - ONE_DAY - 120
}, {
  userName: "Alice",
  data: "Do you have all the equipment? Or do you want me to bring" +
      " my own equipment?",
  timestamp: 1610454723 - ONE_DAY - 130
}, {
  userName: "Alice",
  data: "Please reply ASAP!",
  timestamp: 1610454723 - TWO_MINUTES
}, {
  userName: "Siang",
  data: "Uhmm, Yeah i have my own equipment. Don't worry, just come",
  timestamp: 1610454723
}]

/*
 <Profile> hide if user is same
 <Time>
 <Message>
* */
export const Message = (msg: MessageProps) => {
  let format = " "
  const timeDiff = moment(msg.message.timestamp).diff(msg.prevMessage.timestamp);
  // console.log(timeDiff + " ", msg.prevMessage.timestamp)
  if (msg.first || timeDiff >= ONE_DAY) {
    // console.log("It's more than 5 hours")
    format = "h:mm a, DD/MMM/YYYY"
  } else if (timeDiff >= TWO_MINUTES && timeDiff < ONE_DAY) {
    // console.log("it's more than 2 mins")
    format = "h:mm a"
  }
  console.log("msg: " + msg.message.data)
  return (
      <div className={styles.chatMessageWrapper}>
        {TimeNow(msg.message.timestamp, format)}
        <span
            className={styles.chatMessageSpan}>{msg.message.data}</span>
      </div>
  )
}
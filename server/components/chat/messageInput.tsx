import React, {FormEvent, useState} from "react";
import styles from "styles/components/chat.module.scss"

interface MessageInputProps {
  sendMessage: (text: FormEvent<HTMLInputElement> | React.MouseEvent<HTMLButtonElement, MouseEvent>) => void
}

const MessageInput = (props: MessageInputProps) => {
  const [message, setMessage] = useState<string>()
  const handleSend = (e: FormEvent<HTMLInputElement> | React.MouseEvent<HTMLButtonElement, MouseEvent>) => {
    e.preventDefault()
    props.sendMessage(e)
    setMessage("")
  }
  return (
      <form className={styles.chatMessageInputForm}>
        <div className={styles.chatMessageInputWrapper}>
          <input
              className={styles.chatMessageInput}
              type="text"
              placeholder="Type a message..."
              value={message}
              onChange={({target: {value}}) => setMessage(value)}
              onKeyPress={e => e.key === 'Enter' ? handleSend(e) : null}
          />
          <button className={styles.chatMessageInputButton}
                 type={"button"}
                 onClick={handleSend}>Send</button>
        </div>
      </form>

  )
}

export default MessageInput;
import React from "react"
import styles from "styles/components/tag.module.scss"
import {MissionStatus} from "components/missionsComponent";

export interface TagProps {
  text?: string
  fontSize?: string
}

export const StatusTagComponent: (tp: TagProps) => JSX.Element = (tp: TagProps) => {
  let tagClassName;
  if (tp.text === MissionStatus.COMPLETED) {
    tagClassName = styles.tagStatusCompleted
  } else if (tp.text === MissionStatus.HIRING) {
    tagClassName = styles.tagStatusHiring
  } else if (tp.text === MissionStatus.ON_GOING) {
    tagClassName = styles.tagStatusOnGoing
  }

  return <span className={`${tagClassName}`} style={{
    fontSize: tp.fontSize
  }}>{tp.text}</span>
}

export const PriceTagComponent: (ptp: TagProps) => JSX.Element = (ptp: TagProps) => {
  return <span className={styles.priceTag} style={{
    fontSize: ptp.fontSize
  }}>{ptp.text}</span>
}

export const LocationTagComponent: (ptp: TagProps) => JSX.Element = (ptp: TagProps) => {
  return <span className={styles.locationTag} style={{
    fontSize: ptp.fontSize
  }}>{ptp.text}</span>
}

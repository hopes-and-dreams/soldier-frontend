import React, {CSSProperties} from "react";
import {IoArrowBackOutline as BackArrow} from "react-icons/io5";
import {useRouter} from "next/router";

export const BackButton: React.FC = () => {
  const router = useRouter()
  const shareIconStyle: CSSProperties = {
    color: "black",
    fontSize: "4vw"
  }
  const handleBack = () => {
    console.log("go back")
    router.back()
  }

  return <BackArrow style={shareIconStyle} onClick={handleBack}/>
}

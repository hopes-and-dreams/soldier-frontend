import React, {ChangeEvent, useState} from "react";
import styles from "styles/components/photo-add.module.scss"
import {Photo} from "../views/photo-view";
import {ID_ADD_PHOTO} from "../../commons/constants";
import {PhotoAddView} from "../views/photo-add-view";

export interface addPhotosProps {
  handleSetPhotos: (photo: Photo) => void
  showHide: boolean
  handleSetShowHide: (open: boolean) => void
}

export const AddPhotoSmall: (props: addPhotosProps) => JSX.Element = (props: addPhotosProps) => {
  const [file, setFile] = useState<string>("")
  const handleInputChange = (event: ChangeEvent<HTMLInputElement>) => {
    setFile(URL.createObjectURL(event.target.files![0]));
    props.handleSetShowHide(true)
  }
  return (
      <>
        {props.showHide ? <PhotoAddView url={file}
                                        handleSetShowHide={props.handleSetShowHide}
                                        handleSetPhotos={props.handleSetPhotos}/> :
            <div className={styles.addPhotosWrapper}>
              <input id={ID_ADD_PHOTO} type="file"
                     onChange={handleInputChange} hidden/>
              <label htmlFor={ID_ADD_PHOTO}
                     className={styles.addPhotosLabel}><span>Add Photos</span></label>
            </div>
        }
      </>
  )

}
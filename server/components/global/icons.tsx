import React, {CSSProperties} from "react";
import RankIconSvg from "static/icons/rank2.svg";
import StarIconSvg from "static/icons/rating-star.svg";
import SoldierIconSvg from "static/icons/soldier-icon.svg";
import ChatIconSvg from "static/icons/chat-icon.svg";
import styles from "styles/components/icons.module.scss"

interface IconStyle {
  width: string,
  wrapper?: {
    margin?: string,
    width?: string,
    position?: "relative" | "absolute" | "fixed",
  }
  height?: string,
  color?: string,
}

export const RankIcon: React.FC<IconStyle> = (style: IconStyle) => {
  const styling: CSSProperties = {
    margin: style.wrapper?.margin,
    width: style.wrapper?.width,
    position: style.wrapper?.position,
  }
  return (
      <div style={styling}>
        <RankIconSvg stroke={styles.primaryColor}
                     strokeWidth={"5.2917"}/>
      </div>
  )
}

export const StarRatingIcon: React.FC<IconStyle> = (style: IconStyle) => {
  const styling: CSSProperties = {
    margin: style.wrapper?.margin,
    width: style.wrapper?.width,
    position: style.wrapper?.position,
  }
  return (
      <div style={styling}>
        <StarIconSvg width={style.width}
                     height={style.height === null ? style.width : style.height}
                     fill={styles.primaryColor}/>
      </div>)
}

export const ChatIcon: React.FC<IconStyle> = (style: IconStyle) => {
  const styling: CSSProperties = {
    margin: style.wrapper?.margin,
    width: style.wrapper?.width,
    position: style.wrapper?.position,
  }
  return (
      <div style={styling}>
        <ChatIconSvg width={style.width}
                     height={style.height === null ? style.width : style.height}
                     fill={style.color}/>
      </div>)
}


export const SoldierIcon: React.FC<IconStyle> = (style: IconStyle) => {
  const styling: CSSProperties = {
    margin: style.wrapper?.margin,
    width: style.wrapper?.width,
    position: style.wrapper?.position,
  }
  return (
      <div style={styling}>
        <SoldierIconSvg width={style.width}
                        height={style.height === null ? style.width : style.height}/>
      </div>
  )
}
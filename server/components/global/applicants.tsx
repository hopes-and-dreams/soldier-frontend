import React from "react";
import {SoldierIcon} from "./icons";
import styles from "styles/applicants.module.scss"

interface ApplicantProps {
  total: number
  size: string
}

export const Applicant: (ap: ApplicantProps) => JSX.Element = (ap: ApplicantProps) => {
  return (
      <div className={styles.applicantsMainWrapper}>
        <SoldierIcon width={"100%"} wrapper={{
          width: ap.size
        }}/>
        <span>{ap.total}</span>
      </div>
  )
}
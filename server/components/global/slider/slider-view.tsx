// Only responsible for showing photos

import {animated, useSprings} from "react-spring";
import {SPR_CONF_RESPONSIVE} from "../../../commons/constants";
import {Photo} from "../../views/photo-view";
import {useDrag} from "react-use-gesture";
import {Clamp} from "../../../utils";
import React, {useEffect, useState} from "react";
import styles from "styles/components/photo-slider.module.scss";

interface sliderViewProps {
  addPhotoComponentSmall: JSX.Element | null
  index: number
  sliderComponents: Array<Photo | JSX.Element>
  showHide: boolean
  handleSetPhotos: (photo: Photo) => void
  handleSetShowHide: (open: boolean) => void
  handleImageClickView: (open: boolean, index: number) => void
}

let vw: number;
let swipeDistance: number;
export const SliderView = (props: sliderViewProps) => {
  const [index, setIndex] = useState(0)
  const isPhotoComponentExist = props.addPhotoComponentSmall == null ? 0 : 1;
  const sliderSize = props.sliderComponents.length + isPhotoComponentExist;
  const [images, setImages] = useSprings(sliderSize, (i) => {
    return {
      x: i * vw,
      y: 0,
      config: SPR_CONF_RESPONSIVE,
    }
  });


  const bind: Function = useDrag(({
                                    down,
                                    movement: [mx, my],
                                    direction: [xDir, yDir],
                                    distance,
                                    cancel,
                                  }) => {
    if (down && distance > swipeDistance && cancel != null) {
      console.log("cal")
      cancel()
      setIndex(Clamp(index + (xDir > 0 ? -1 : 1), 0, sliderSize - (1 - isPhotoComponentExist)));
    }

    // @ts-ignore
    setImages((i) => {
      const x = (i - index) * vw + (down ? mx : 0)
      return {x}
    })
  })

  useEffect(() => {
    vw = Math.max(document.documentElement.clientWidth || 0)
    swipeDistance = vw / 4
  }, [])
  console.log("SliderView showHide: " + props.showHide)

  const sliderImages = images.map(({x}, i) => {
    const photos: Photo[] = props.sliderComponents as Photo[]
    console.log("index: " + i + "sliderComponents: " + photos.length)
    return (
        <>
          <animated.div  {...bind()} key={i} style={{x}}
                         className={styles.photoSliderImgContainer}>
            {i == photos.length ? props.addPhotoComponentSmall :
                <img src={photos[i].src}
                     alt={photos[i].caption}
                     className={styles.photoSliderImg}
                     onClick={() => props.handleImageClickView(true, i)}/>}

          </animated.div>
        </>
    )
  })

  return (
      <>
        {sliderImages}
      </>
  )
}
import React, {useEffect, useState} from "react";
import styles from "styles/components/photo-slider.module.scss"
import {SliderView} from "./slider-view";
import {Photo, PhotoViewer} from "../../views/photo-view";
import {AddPhotoSmall} from "../add-photo-small";

let vw: number;

interface SliderPhotoProps {
  photos?: Photo[]
  sliderOnly: boolean
  height: string
}

export const PhotoSlider = (props: SliderPhotoProps) => {
  const [index, setIndex] = useState(0)
  const [showPhotoAdd, setShowPhotoAdd] = useState(false)
  const [showImgClick, setShowImgClick] = useState(false)
  const [photos, setPhotos] = useState<Photo[] | undefined>(props.photos)
  const handleSetPhotos = (photo: Photo) => {
    photos!.push(photo)
    setPhotos(photos)
    setShowPhotoAdd(false)
    setShowImgClick(false)
    console.log("Handle Set Photo", photos)
  }
  const handleSetShowHide = (open: boolean) => {
    console.log("handleSetShowHide: ")
    setShowPhotoAdd(open)
  }
  const handleImageClickView = (open: boolean, index: number) => {
    console.log("handleImageClickView: " + open)
    setIndex(index)
    setShowImgClick(open)
  }

  useEffect(() => {
    vw = Math.max(document.documentElement.clientWidth || 0)
  }, [])

  return (
      <div className={styles.photoSliderWrapper}
           style={{height: props.height}}>
        <PhotoViewer photos={photos} currentIndex={index}
                     show={showImgClick}
                     handleImageClickView={handleImageClickView}/>
        <SliderView sliderComponents={photos!} index={index}
                    addPhotoComponentSmall={props.sliderOnly ?
                        null : <AddPhotoSmall
                            handleSetPhotos={handleSetPhotos}
                            showHide={showPhotoAdd}
                            handleSetShowHide={handleSetShowHide}/>}
                    showHide={showPhotoAdd}
                    handleSetShowHide={handleSetShowHide}
                    handleSetPhotos={handleSetPhotos}
                    handleImageClickView={handleImageClickView}
        />
      </div>
  )
}
import React from "react";
import styles from "styles/components/time.module.scss"
import moment from "moment";

interface TimeProps {
  timestamp: number
}

export const TimeFromNow: (t: TimeProps) => JSX.Element = (t: TimeProps) => {
  return (
      <div className={styles.timeMainWrapper}>
        <span>{moment.unix(t.timestamp).fromNow()}</span>
      </div>
  )
}

export const TimeNow: (t: number, format: string) => JSX.Element = (t: number, format: string) => {
  console.log(t)
  return (
      <div className={styles.timeMainWrapper}>
        <span>{moment.unix(t).format(format)}</span>
      </div>
  )
}
import React from "react";
import styles from "styles/userProfile.module.scss";
import {MissionProps} from "components/missionsComponent";
import {GetPriceString} from "utils";
import {
  PriceTagComponent,
  StatusTagComponent
} from "components/global/tag";
import {Applicant} from "components/global/applicants";
import {TimeFromNow} from "components/global/time";

interface ProfileMissionsProp {
  missions: MissionProps[]
}

export const ProfileMissions: React.FC<ProfileMissionsProp> = (missionProps) => {
  const handleProfileMissionsClick = () => {
  }
  const missions = missionProps.missions.map((mission) => {
    return (
        <div className={styles.profileMissionsWrapper}
             onClick={handleProfileMissionsClick}>
          <div className={styles.profileMissionsStatusPriceWrapper}>
            {StatusTagComponent({
              text: mission.status,
              fontSize: styles.locationTagFs
            })}
            {PriceTagComponent({
              text: GetPriceString(mission.price),
              fontSize: styles.priceTagFs
            })}
          </div>
          <h4 className={styles.profileMissionsH4}>{mission.title}</h4>
          <p className={styles.profileMissionsP}>{mission.missionInfo.description}</p>
          <div className={styles.profileMissionsMomentMainWrapper}>
            <Applicant size={styles.applicantSize}
                       total={mission.applicants}/>
            <TimeFromNow timestamp={mission.createdAt}/>
          </div>
        </div>
    )
  })
  return (
      <div className={styles.profileMissionsMainWrapper}>
        {missions}
      </div>
  );
}
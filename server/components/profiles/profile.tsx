import React, {useContext, useState} from "react";
import styles from "styles/userProfile.module.scss"
import {JWT_COOKIE_NAME, PROFILE_COMPONENTS} from "commons/constants";
import {Photo, PhotoViewer} from "components/views/photo-view";
import {RankIcon} from "components/global/icons";
import {Gallery} from "components/profiles/gallery";
import {MissionProps} from "components/missionsComponent";
import {ProfileMissions} from "./profiles-mission";
import {FilterMissions} from "utils";
import {useCookies} from "react-cookie";
import {AuthStore} from "../../store/auth-store";

export interface Address {
  city: string
  state: string
}

export interface Profile {
  profileImage: Photo[]
  aboutMe: string
  address: Address
  name: string
  level: number
  profileId: string
  email: string
  missionList: MissionProps[]
}

export enum Filter {
  ALL_TASK = "All Tasks",
  COMPLETED = "Completed",
  HIRING = "Hiring",
  ON_GOING = "On Going",
}

export interface ProfileProps {
  profileInfo: Profile
  profileImages: Photo[]
}

export const mockedProfile: Profile = {
  name: "Rachel Lai",
  level: 1,
  aboutMe: "Professional Illustrator, Animator, and Logo Designer.",
  profileId: "2323400892389042",
  profileImage: [{
    src: "https://i.pinimg.com/originals/7a/07/75/7a07759bea9f1aee3daf38b1604d26ac.jpg",
    caption: "a fcking caption, hahaa"
  }],
  missionList: [],
  email: "herodotus94@gmail.com",
  address: {
    city: "Pasir Gudang",
    state: "Johor"
  }
}

const galleryImages = [
  {
    src: "https://i.pinimg.com/originals/7a/07/75/7a07759bea9f1aee3daf38b1604d26ac.jpg",
    caption: "a caption"
  },
  {
    src: "https://i.pinimg.com/originals/0f/a6/22/0fa62255fa1735fd729319b4769246f7.jpg",
    caption: "a 94923490 i9fsdsdfaodjilakdjaldkja lsdkjd laskdj" +
        " aldkzxj dldkj zaslkkj lkasj dslakdalkdj asldajs ldjxk" +
        " djzxld "
  },
  {
    src: "https://i.pinimg.com/originals/0f/a6/22/0fa62255fa1735fd729319b4769246f7.jpg",
    caption: "a captionasld jad ja"
  },
  {
    src: "https://i.pinimg.com/originals/0a/37/2c/0a372cf3d6c27eab388ca1d075f6bc29.jpg",
    caption: "a caption asld 12"
  },
  {
    src: "https://i.pinimg.com/originals/7a/07/75/7a07759bea9f1aee3daf38b1604d26ac.jpg",
    caption: "a caption"
  },
  {
    src: "https://i.pinimg.com/originals/7a/07/75/7a07759bea9f1aee3daf38b1604d26ac.jpg",
    caption: "a caption"
  },
  {
    src: "https://i.pinimg.com/originals/7a/07/75/7a07759bea9f1aee3daf38b1604d26ac.jpg",
    caption: "a caption"
  },
  {
    src: "https://images.pexels.com/photos/747964/pexels-photo-747964.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500",
    caption: "a caption"
  },
]

const Profiles: React.FC<ProfileProps> = ({profileInfo, profileImages}: ProfileProps) => {
  let [missionsFilterStatus, setMissionsFilterStatus] = useState(Filter.ALL_TASK);
  const [cookie, setCookie] = useCookies([JWT_COOKIE_NAME]);
  const authContext = useContext(AuthStore)
  const [showPhotoViewer, setShowPhotoViewer] = useState<boolean>(false);
  const [photos, setPhotos] = useState<Photo[]>([]);
  const [index, setIndex] = useState<number>(0);
  console.log("profileProps", profileInfo)
  const src = profileImages[0].src
  const caption = profileImages[0].caption
  const address = profileInfo.address || mockedProfile.address
  const missions = profileInfo.missionList || mockedProfile.missionList
  const name = profileInfo.name
  const aboutMe = profileInfo.aboutMe
  const email = profileInfo.email
  const handlePhoto = (photos: Photo[], index: number) => {
    console.log("handlePhoto", index)
    setPhotos(photos)
    setIndex(index)
    setShowPhotoViewer(true)
  }
  const handleFilterClick = () => {
    // three states toggle switch
    if (missionsFilterStatus === Filter.ALL_TASK || missionsFilterStatus === undefined) {
      setMissionsFilterStatus(Filter.ON_GOING)
    } else if (missionsFilterStatus === Filter.ON_GOING) {
      setMissionsFilterStatus(Filter.HIRING)
    } else if (missionsFilterStatus === Filter.HIRING) {
      setMissionsFilterStatus(Filter.COMPLETED)
    } else if (missionsFilterStatus === Filter.COMPLETED) {
      setMissionsFilterStatus(Filter.ALL_TASK)
    }
  }
  const handleCloseView = (isOpen: boolean) => {
    setPhotos([])
    setIndex(0)
    setShowPhotoViewer(false)
  }

  return (
      <div id={PROFILE_COMPONENTS}
           className={styles.profileMainComponent}>
        <PhotoViewer photos={photos} currentIndex={index}
                     show={showPhotoViewer}
                     handleImageClickView={handleCloseView}/>
        {/* Upper info part*/}
        <div className={styles.profileUpperInfoPartWrapper}>
          <div className={styles.profileInfoWrapper}>
            <img src={src}
                 alt={caption}
                 className={styles.profileImage}
                 onClick={() => handlePhoto(profileInfo.profileImage, 0)}/>
            <div className={styles.profileInfo}>
              <div className={styles.profileNameRankWrapper}>
                {/* long malay name is 29 chars*/}
                <h3>{name}</h3>
                <RankIcon width={"100%"} wrapper={{
                  width: styles.rankWidth,
                  margin: "0 0 0 2vw"
                }}/>
              </div>
              <p>{aboutMe}</p>
            </div>
          </div>
          <div className={styles.profileReviewButtonWrapper}>
            <span>{authContext.isAuthorized(cookie)}</span>
            <span>{`${address.city}, ${address.state}`}</span>
          </div>
        </div>
        {/* Missions */}
        <div className={styles.titleFilterWrapper}>
          <h3>Missions</h3>
          <button
              onClick={handleFilterClick}>{missionsFilterStatus}</button>
        </div>
        <div className={styles.profileMissionsBackground}>
          <ProfileMissions
              missions={FilterMissions(missions, missionsFilterStatus, missionsFilterStatus !== Filter.ALL_TASK)}/>
        </div>
        {/* Gallery */}
        <h3 className={styles.profileGalleryH3}>Gallery</h3>
        <div className={styles.profileGalleryWrapper}>
          <Gallery photos={galleryImages}
                   handlePhoto={handlePhoto}/>
        </div>
      </div>
  )
}

export default Profiles;
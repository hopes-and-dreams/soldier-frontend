import React from "react";
import Link from "next/link";

export interface ProfilePictureProps {
  link: string
  src: string
  caption: string
  width: string
}

const styleImg = {
  width: "100%",
  height: "100%",
  borderRadius: "100%"
}

const ProfilePicture: (props: ProfilePictureProps) => JSX.Element = (props: ProfilePictureProps) => {
  const styleImgContainer = {
    width: props.width,
    height: props.width
  }
  return (
      <Link href={props.link}>
        <div style={styleImgContainer}>
          <img src={props.src} alt={props.caption} style={styleImg}/>
        </div>
      </Link>
  )
}

export default ProfilePicture;
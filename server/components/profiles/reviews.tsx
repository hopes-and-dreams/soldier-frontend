// @ts-ignore
import React from "react";
import styles from "../../styles/components/reviews.module.scss";
import {Profile} from "./profile";
import {observer} from "mobx-react";
import RankIcon from "static/icons/rank2.svg";
import StarRatingIcon from "static/icons/rating-star.svg";
import Link from "next/link";

export interface Reviews {
  reviewer: Profile
  rating: number
  message: string
}

const mockedReviews: Reviews[] = [{
  reviewer: {
    profileImage: [{
      src: "https://images.pexels.com/photos/747964/pexels-photo-747964.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500",
      caption: "a caption"
    }],
    name: "Elizabeth John Wick",
    level: 12,
    aboutMe: "I'm a potato.",
    profileId: ""
  },
  rating: 4.9,
  message: "She is the best designer I know!!! 100% I would like to work with her again! She was really attentive and reply to my enquiries almost immediately"
}]

const ReviewsContentComponent: React.FC<Reviews> = ({reviewer, rating, message}) => {
  return (
      <div className={styles.profileReviewWrapper}>
        <div className={styles.profileReviewReviewerWrapper}>
          {/* Image */}
          <div className={styles.profileReviewImgWrapper}>
            <img
                src={"https://i.pinimg.com/originals/0f/a6/22/0fa62255fa1735fd729319b4769246f7.jpg"}
                className={styles.profileReviewImg}
                alt={"dont forget the alt"}/>
          </div>
          <div className={styles.profileReviewNameRatingWrapper}>
            <div className={styles.profileRevReviewerWrapper}>
              <h2>Elizabeth Jolin</h2>
              <RankIcon width={"100%"} wrapper={{
                margin: "0 0 0 2vw",
                width: "5vw",
                position: "relative"
              }}/>
            </div>
            {/* Star rating */}
            <div className={styles.profileReviewRatingWrapper}>
              <StarRatingIcon width={"100%"} wrapper={{
                margin: "0 1vw 0 0",
                width: "3vw",
                position: "relative"
              }}/>
              <span
                  className={styles.profileReviewRatingSpan}>4.8</span>
              <span className={styles.profileReviewTimeSpan}>23 October 2020</span>
              <Link href={"/missions/test"}>
                <a className={styles.profileReviewMission}>Help me
                  design a logo for my
                  nasi Lemak Stall </a></Link>
            </div>
          </div>
        </div>
        <p className={styles.profileReviewP}>She is the best designer
          I know!!! 100% I would like to work with her
          again! She was really attentive and reply to my enquiries
          almost immediately</p>
      </div>
  )
}

export const ReviewsComponent: React.FC = observer(() => {
  return (
      <div className={styles.reviewsContentWrapper}>
        {mockedReviews.map(rev => {
          return <ReviewsContentComponent reviewer={rev.reviewer}
                                          rating={rev.rating}
                                          message={rev.message}/>
        })}
      </div>
  )
})

export const ReviewsPage: React.FC = observer(() => {
  return (
      <div className={styles.reviewsMainWrapper}>
        <div className={styles.reviewsTitleWrapper}>
          <div className={styles.profileRatingIconHeaderWrapper}>
            <StarRatingIcon width={"100%"} wrapper={{
              width: "4vw",
              position: "relative",
              margin: "auto 1vw auto 0"
            }}/>
          </div>
          <h3 className={styles.reviewsTitle}>{`${4.66} (${1} Reviews)`}</h3>
        </div>
        <ReviewsComponent/>
      </div>
  )
});

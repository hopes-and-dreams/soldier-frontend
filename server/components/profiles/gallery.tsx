import React from "react";
import styles from "styles/userProfile.module.scss";
import {Photo} from "components/views/photo-view";

interface GalleryProps {
  photos: Photo[]
  handlePhoto: (photos: Photo[], index: number) => void
}


export const Gallery: React.FC<GalleryProps> = ({photos, handlePhoto}) => {

  const galleryImagesDom = photos.map((photo, i) => {
    return (
        <div className={styles.profileGalleryImageWrapper}>
          <img src={photo.src} alt={photo.caption}
               className={styles.profileGalleryImage}
               onClick={() => handlePhoto(photos, i)}/>
        </div>
    )
  })
  return (
      <div className={styles.profileGalleryImageMainWrapper}>
        {galleryImagesDom}
      </div>
  );
}
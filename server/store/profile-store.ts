import {createContext} from "react";
import {Profile} from "../components/profiles/profile";

class profileStore {
  profile: Profile = {
    email: "",
    missionList: [],
    address:
        {
          state: "",
          city: "",
        },
    profileImage: [],
    profileId: "",
    name: "",
    level: 0,
    aboutMe: "",

  }
}

export const ProfileStore = createContext(new profileStore());
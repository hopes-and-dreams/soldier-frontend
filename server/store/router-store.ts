import {useRouter} from "next/router";
import {
  MISSIONS_URL,
  MISSIONS_URL_ID,
  PROFILES_URL,
  PROFILES_URL_ID
} from "../commons/constants";
import {createContext} from "react";
import {action} from "mobx";

class routerStore {
  @action getHeaderIndex = () => {
    const router = useRouter()
    switch (router.pathname) {
      case MISSIONS_URL:
      case MISSIONS_URL_ID:
        return 0
      case PROFILES_URL:
      case PROFILES_URL_ID:
        return 1
    }
    return 0
  }
}

export const RouterStore = createContext(new routerStore());
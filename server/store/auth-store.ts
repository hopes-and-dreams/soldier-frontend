import {createContext} from "react";
import {Cookie} from "universal-cookie";

export type authState =
    "NOT_AUTHORIZED"
    | "AUTHENTICATING"
    | "AUTHORIZED";

class authStore {
  private jwtToken: string;

  isAuthorized = (cookie: Cookie) => {
    console.log("isAuthorized", cookie)
    return cookie.toString()
  }

  isOwner = (cookie: string) => {
    console.log("isOwner: ", cookie)

  }

}

export const AuthStore = createContext(new authStore())
console.log("AuthStore got initialized", AuthStore)
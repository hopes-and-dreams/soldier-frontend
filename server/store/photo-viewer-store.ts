import {action, observable} from "mobx";
import {createContext} from "react";
import {Photo} from "components/views/photo-view";

interface PhotoViewerProps {
  isOpen: boolean
  photos?: Photo[]
  currentIndex: number
}


class photoViewerStore {
  @observable photo: PhotoViewerProps = {
    isOpen: false,
    photos: [],
    currentIndex: 0,
  }

  @action updatePhotos = (index: number, photos?: Photo[]) => {
    this.photo.photos = photos;
    this.photo.currentIndex = index;
    this.photo.isOpen = true
  }

  @action updateCurrentIndex = (index: number) => {
    this.photo.currentIndex = index;
  }

  @action updateOpenState = (open: boolean) => {
    this.photo.isOpen = open;
  }
}

export const PhotoViewerStore = createContext(new photoViewerStore())
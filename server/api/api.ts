import {PORT_REST, PORT_WS, SERVER_HOME} from "../commons/constants";
import axios, {AxiosInstance, AxiosRequestConfig} from "axios";

class api {
  private axiosRest: AxiosInstance;
  private axiosWs: AxiosInstance;

  constructor() {
    this.axiosRest = axios.create({
      baseURL: `http://${SERVER_HOME}:${PORT_REST}`,
      withCredentials: true
    })
    this.axiosWs = axios.create({
      baseURL: `http://${SERVER_HOME}:${PORT_WS}`,
      withCredentials: true
    })
  }

  // Rest
  postRest = async (path: string, headers?: any, body?: any) => {
    console.log("postRest: " + path, headers)
    // return axios("http://localhost:8880/", {
    //   method: "POST",
    //   headers: headers || {},
    //   data: body || {},
    // });
    return this.axiosRest.post(path, body, {headers: headers})
  }
  getRest = (path: string, headers?: AxiosRequestConfig) => {
    console.log("getRest: " + path, headers)
    return this.axiosRest.get(path, headers)
  }

  // Websocket
  getWs = (path: string, headers?: AxiosRequestConfig) => {
    console.log("getWs: " + path, headers)
    return this.axiosWs.get(path, headers)
  }
}

export const Api = new api();

import React, {useRef} from "react";
import Missions from "./missions";

const Home = () => {
  console.log("Home FC initialized!")
  const ref = useRef<HTMLDivElement>(null)

  return (
      <Missions/>
  )
}

export default Home;

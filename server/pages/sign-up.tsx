import styles from "../styles/missions.module.scss";
import React from "react";
import SignUpPage from "../components/auth/sign-up-page";

const SignUp = () => {
  return (
      <div className={styles.missionMainWrapper}>
        <SignUpPage/>
      </div>
  )
}

export default SignUp;

import styles from "styles/missions.module.scss";
import React from "react";
import LoginPage from "components/auth/login-page";

const EmailLogin = () => {
  return (
      <div className={styles.missionMainWrapper}>
        <LoginPage/>
      </div>
  )
}

export default EmailLogin;

import React from "react";
import {Header} from "../components/header";
import MissionsComponent from "../components/missionsComponent";
import styles from "styles/missions.module.scss"

const Missions = () => {
  return (
      <div className={styles.missionMainWrapper}>
        <Header/>
        <MissionsComponent/>
      </div>
  )
}

export default Missions;

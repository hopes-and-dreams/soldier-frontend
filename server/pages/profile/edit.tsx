import React, {useContext, useState} from "react";
import {observer} from "mobx-react";
import styles from "styles/account-settings.module.scss";
import {MissionViewerStore} from "store/mission-viewer-store";
import TextField from '@material-ui/core/TextField';

export enum Gender {
  MALE = "Male",
  FEMALE = "Female"
}

const getTitle = (title: string) => {
  return <h3 className={styles.accountTitleH3}>{title}</h3>
}

const EditComponent: React.FC = observer(() => {
  const missionViewStore = useContext(MissionViewerStore);
  let savedBody;
  const [name, setName] = useState("");
  const [gender, setGender] = useState("");
  const [birthDate, setBirthDate] = useState("");
  const [email, setEmail] = useState("");
  const [phoneNumber, setphoneNumber] = useState("");
  const [address, setAddress] = useState("");
  const [aboutMe, setAboutMe] = useState("");
  const handleSave = () => {
    // check all required field
    if (true) {

    }
    savedBody = {
      name: name,
      gender: gender,
      birthDate: birthDate,
      email: email,
      phoneNumber: phoneNumber,
      address: address,
      aboutMe: aboutMe,
    }
    console.log(savedBody);
  }
  const handleGenderClick = () => {
    setGender(
        gender === Gender.FEMALE || gender === "" ? Gender.MALE : Gender.FEMALE);
  }
  const datePickerStyle = {
    marginBottom: `${styles.paddingValue}`,
  }

  return (
      <div className={styles.accountSettingWrapper}>
        <button onClick={handleSave}
                className={styles.accountSaveButton}>Save
        </button>
        <h3 className={styles.accountSettingsH3}>Personal
          information</h3>
        {getTitle("Name")}
        <input name={"Name"} onChange={e => setName(e.target.value)}
               className={styles.accountInfoInput}/>
        {getTitle("Gender")}
        <input readOnly={true} name={"Gender"} value={gender}
               placeholder={"Tap to change"}
               onClick={handleGenderClick}
               className={styles.accountInfoInput}/>
        {getTitle("Birth date")}
        <TextField inputProps={{
          style: {
            fontSize: `${styles.fontSizeInput}`,
            padding: 0,
          }
        }} id={"date"} type={"date"} defaultValue={"1999-12-31"}
                   style={datePickerStyle}/>
        {/*<input name={"Birth date"} placeholder={"yyyy/mm/dd"} onChange={e => setName(e.target.value)}*/}
        {/*       className={styles.accountName}/>*/}
        {getTitle("Email")}
        <input name={"Email"} onChange={e => setName(e.target.value)}
               className={styles.accountInfoInput}/>
        {getTitle("Phone Number")}
        <input name={"Phone Number"}
               onChange={e => setName(e.target.value)}
               className={styles.accountInfoInput}/>
        <h3 className={styles.accountAddressTitle}>Address</h3>
        <input name={"Address"}
               onChange={e => setName(e.target.value)}
               className={styles.accountAddress}/>
        {getTitle("Who Am I")}
        <div className={styles.accountTextAreaWrapper}>
          <textarea name={"Who Am I"} spellCheck={false}
                    onChange={e => setName(e.target.value)}
                    className={styles.accountTextArea}/>
        </div>
      </div>
  )
})

const Edit: React.FC = observer(() => {
  return (
      <div className={styles.accountSettingsMainWrapper}>
        <EditComponent/>
      </div>
  )
});
export default Edit;
import React from "react";
import {Header} from "../../components/header";
import Profiles, {
  mockedProfile,
  ProfileProps,
} from "../../components/profiles/profile";
import styles from "styles/userProfile.module.scss"
import {GetServerSideProps} from "next";

const MainUserProfile = (props: ProfileProps) => {
  console.log("MainUserProfile: ", props)
  if (props == undefined) {
    return <div/>
  }
  return (
      <div className={styles.profileMainWrapper}>
        <Header/>
        <Profiles profileInfo={props.profileInfo}
                  profileImages={props.profileImages}/>
      </div>
  )
}

export const getServerSideProps: GetServerSideProps = async (context) => {
  console.log(context.query)
  return {
    props: {
      profileInfo: mockedProfile,
      profileImages: mockedProfile.profileImage
    }
  }
}

export default MainUserProfile;
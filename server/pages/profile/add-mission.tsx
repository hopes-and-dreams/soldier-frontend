import React, {ChangeEvent, useContext, useState} from "react";
import {observer} from "mobx-react";
import styles from "styles/components/add-mission.module.scss";
import {MissionViewerStore} from "store/mission-viewer-store";
import {Photo} from "../../components/views/photo-view";
import {PhotoSlider} from "../../components/global/slider/slider-photo";

const getTitle = (title: string) => {
  return <h3 className={styles.addMissionTitleH3}>{title}</h3>
}

const AddPhotosComponent = () => {
  const [photos, setPhotos] = useState("");
  return (
      <div>
      </div>
  )
}

const AddMissionComponent: React.FC = () => {
  const missionViewStore = useContext(MissionViewerStore);
  let savedBody;
  let photos: Photo[] = [
    {
      src: "https://i.pinimg.com/originals/7a/07/75/7a07759bea9f1aee3daf38b1604d26ac.jpg",
      caption: "a caption"
    },
    {
      src: "https://images.pexels.com/photos/747964/pexels-photo-747964.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500",
      caption: "a caption"
    },
  ];
  const [name, setName] = useState("");
  const [remote, setRemote] = useState(false);
  const [city, setCity] = useState("");
  const [state, setState] = useState("");
  const [missionDescription, setMissionDescription] = useState("");

  // Photo States
  const [showHide, setShowHide] = useState(false);
  const [photoView, setPhotoView] = useState(false);
  const [file, setFile] = useState("");
  const handleSave = () => {
    // check all required field
    savedBody = {
      name: name,
      remote: remote,
      city: city,
      state: state,
      missionDescription: missionDescription,
    }
    console.log(savedBody);
  }
  const handleFileUpload = (event: React.FormEvent<HTMLInputElement>) => {
    setFile(URL.createObjectURL((event.currentTarget.files![0])));
    setShowHide(true);
  }
  const handleGalleryImgClick = (index: number) => {

  }
  const handleInputChange = (event: ChangeEvent<HTMLInputElement>) => {
    setFile("ha")
  }


  return (
      <>
        <div className={styles.addMissionWrapper}>
          <button onClick={handleSave}
                  className={styles.addMissionButton}>Add Mission
          </button>
          <h3 className={styles.accountSettingsH3}>Add Mission</h3>
          {getTitle("Title")}
          <input name={"Title"}
                 onChange={e => setName(e.target.value)}
                 className={styles.inputOneLine}/>
          <label
              className={styles.addMissionRemoteWrapper}>{getTitle("Remote")}
            <input name={"Remote"} type={"checkbox"}
                   onChange={e => setRemote(e.target.checked)}/>
          </label>
          <div className={styles.addMissionLocationWrapper}>
            <div>
              <h3 className={remote ? styles.addMissionTitleH3Disabled : styles.addMissionTitleH3}>City</h3>
              <input name={"City"} type={"text"}
                     onChange={e => setName(e.target.value)}
                     className={remote ? styles.inputBoxDisabled : styles.inputBox}
                     disabled={remote}/>
            </div>
            <div>
              <h3 className={remote ? styles.addMissionTitleH3Disabled : styles.addMissionTitleH3}>State</h3>
              <input name={"State"} type={"text"}
                     onChange={e => setName(e.target.value)}
                     className={remote ? styles.inputBoxDisabled : styles.inputBox}
                     disabled={remote}/>
            </div>
          </div>
          <PhotoSlider sliderOnly={false} photos={[]} height={"5em"}/>
          {getTitle("Mission Description")}
          <div className={styles.accountTextAreaWrapper}>
          <textarea name={"Mission Description"} spellCheck={false}
                    onChange={e => setName(e.target.value)}
                    className={styles.accountTextArea}/>
          </div>
        </div>
      </>
  )
}

const AddMission: React.FC = observer(() => {
  return (
      <div className={styles.addMissionMainWrapper}>
        <AddMissionComponent/>
      </div>
  )
});
export default AddMission;
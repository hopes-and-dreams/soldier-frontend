import React, {useContext, useEffect} from "react";
import styles from "../../styles/views/mission-view.module.scss";
import {PhotoViewerStore} from "store/photo-viewer-store";
// @ts-ignore
import SoldierIcon from "static/icons/soldier-icon.svg";
import {StatusTagComponent} from "components/global/tag";
import {GetPriceString} from "utils";
import {ShareButtonComponent} from "components/global/share";
import {mockedMissionProps} from "components/missionsComponent";
import {PhotoSlider} from "../../components/global/slider/slider-photo";

let href: string;
const MissionViewerComponent: React.FC = () => {
  const photoViewStore = useContext(PhotoViewerStore);
  const handleImgClick = () => {
    photoViewStore.updatePhotos(0, mockedMissionProps[0].missionInfo.photos)
    photoViewStore.updateOpenState(true)
  }
  const handleApplyClick = () => {
    console.log("Applied!")
  }

  useEffect(() => {
    href = window.location.href
  }, [])
  return (
      <div className={styles.missionViewMainWrapper}>
        <div className={styles.missionViewWrapper}>
          <h3 className={styles.missionViewTitleH3}>
            {mockedMissionProps[0].title}
          </h3>
          <p className={styles.missionViewInfoP}>{mockedMissionProps[0].missionInfo.description}</p>
          {mockedMissionProps[0].missionInfo.photos === null ?
              <div/> :
              <div className={styles.missionImgWrapper}>
                <div className={styles.missionImgWrapper}>
                  <PhotoSlider sliderOnly={true} height={"10em"}
                               photos={mockedMissionProps[0].missionInfo.photos}/>
                </div>
              </div>}
          <div className={styles.missionTagShareWrapperDiv}>
            {StatusTagComponent({
              text: mockedMissionProps[0].missionAddress
            })}
            {StatusTagComponent({
              text: GetPriceString(mockedMissionProps[0].price)
            })}
            <ShareButtonComponent linkToShare={href}/>
          </div>
          <div className={styles.missionApplyWrapperDiv}>
            <button className={styles.missionApplyButton}
                    onClick={handleApplyClick}><SoldierIcon
                fill={'black'}/><span
                className={styles.missionApplySpan}>Apply</span>
            </button>
          </div>
        </div>
      </div>
  )
}

export default MissionViewerComponent;
import React, {
  FormEvent,
  useContext,
  useEffect,
  useRef,
  useState
} from "react";
import {
  Message,
  MessageTextProps,
} from "components/chat/messageText";
import {Clamp} from "utils";
import ProfilePicture from "components/profiles/ProfilePicture";
import {
  CHAT_VIEW_COMPONENTS,
  PORT_WS,
  PROFILES_URL,
  SERVER_HOME
} from "commons/constants";
import styles from "styles/components/chat.module.scss"
import MessageInput from "components/chat/messageInput";
import {Header} from "components/header";
import {NextPage} from "next";
import moment from "moment";
import {Profile} from "components/profiles/profile";
import {ProfileStore} from "store/profile-store";

interface WsProps {
  radio: string
  receiver: string
  profile: Profile
  error: string
}

const UserInfo: NextPage<Profile> = ({profileImage, name, profileId}: Profile) => {
  return (
      <div className={styles.chatUserInfoWrapper}>
        <ProfilePicture
            width={"5em"}
            caption={profileImage[0].caption}
            src={profileImage[0].src}
            link={PROFILES_URL + profileId}/>
        <span>{name}</span>
      </div>
  )
}

const getChatObject: (data: string, profile: Profile) => MessageTextProps = (data: string, profile: Profile) => {
  const obj: MessageTextProps = {
    userName: profile.name,
    data: data,
    timestamp: moment().unix()
  }
  return obj;
}

const ChattingView: NextPage<Object> = (props: WsProps) => {
  const [msgs, setMsgs] = useState<MessageTextProps[]>([])
  const profileStore = useContext(ProfileStore)
  console.log("props: ", props)
  const webSocket = useRef<WebSocket | null>(null)
  const sendMessageHandler = (event: FormEvent<HTMLInputElement>) => {
    const json = getChatObject(event.currentTarget.value, profileStore.profile)
    webSocket.current!.send(JSON.stringify(json));
  }
  console.log(moment().unix())
  const messages = msgs.map((msg, index) => {
    const prevIndex = Clamp(index - 1, 0, index) // only has min value
    const prevUser = msgs[prevIndex].userName
    const shouldRenderProfile = prevUser === msg.userName
    return (
        <>
          {shouldRenderProfile ?
              <UserInfo {...props.profile}/> : null}
          <Message message={msg} prevMessage={msgs[prevIndex]}
                   first={shouldRenderProfile}/>
        </>
    )
  })
  useEffect(() => {
    if (!props.receiver) {
      return
    }
    webSocket.current = new WebSocket(`ws://${SERVER_HOME}:${PORT_WS}/ws?receiver=${props.receiver}`);
    webSocket.current.addEventListener("open", (event) => {
    })
    webSocket.current.onopen = () => {
      console.log("Successfully Connected to server");
    }
    webSocket.current.onmessage = (message) => {
      const textProps: MessageTextProps = JSON.parse(message.data)
      const messageText = getChatObject(textProps.data, profileStore.profile)
      setMsgs(prevMsgs => [...prevMsgs, messageText])
    }
  }, [])
  return (
      <div className={styles.chatViewMainWrapper}>
        <Header/>
        <div className={styles.chatViewWrapper}>
          <div id={CHAT_VIEW_COMPONENTS}
               className={styles.chatViewMessageWrapper}>
            {messages}
          </div>
          <MessageInput sendMessage={sendMessageHandler}/>
        </div>
      </div>
  )
}


ChattingView.getInitialProps = ({req, query}) => {
  console.log("ChattingView getInitialProps called")
  // try {
  //   const res = await Api.postRest("/ws-ticket",
  //       {
  //         userId: "Rachel Lai",
  //         cookie: req!.headers.cookie,
  //         withCredentials: true
  //       }
  //   )
  //   return {radio: res.data, error: ""}
  // } catch (e) {
  //   console.error(e)
  //   return {
  //     radio: "",
  //     error: e.message
  //   }
  // }
  const {receiverId} = query
  return {
    receiver: receiverId,
  }

}

export default ChattingView;
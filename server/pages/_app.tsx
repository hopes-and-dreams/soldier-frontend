import 'styles/index.scss'
import React from "react";
import {AppProps} from "next/app";
import {LIGHT_THEME, ROOT} from "../commons/constants";

function MyApp({Component, pageProps}: AppProps) {
  return (
      <div id={ROOT} className={`${LIGHT_THEME}`}>
        <Component {...pageProps} />
      </div>
  )
}

export default MyApp

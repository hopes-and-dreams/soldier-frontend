import {MissionProps, Price} from "components/missionsComponent";
import {useEffect, useRef, useState} from "react";
import {
  CHAT_VIEW_COMPONENTS,
  CHAT_VIEW_URL_ID,
  MISSION_COMPONENTS,
  MISSIONS_URL,
  MISSIONS_URL_ID,
  PROFILE_COMPONENTS,
  PROFILES_URL,
  PROFILES_URL_ID
} from "./commons/constants";

export const GetPriceString: (price: Price) => string = (price: Price) => {
  return `${price.currency} ${price.isRate ? price.amount + " / Hour" : price.amount}`
}

export const ConditionalRender: (condition: boolean, ifTrue: Object, ifFalse: Object) => Object = (condition: boolean, ifTrue: Object, ifFalse: Object) => {
  return condition ? ifTrue : ifFalse;
}

export const UsePrevious = (value: any) => {
  const ref = useRef<any>();

  useEffect(() => {
    ref.current = value;
  })
  return ref.current;
}

/**
 * @param missions
 * mission to be filtered
 * @param status
 * status of the mission
 * @param shouldFilter
 * will only filter if shouldFilter is true
 * @return {MissionProps}
 */
export const FilterMissions = (missions: MissionProps[], status: string, shouldFilter: boolean) => {
  return shouldFilter ? missions.filter(mission => mission.status === status) : missions;
}

export const GetRouterIndex = (path: string) => {
  switch (path) {
    case MISSIONS_URL:
    case MISSIONS_URL_ID:
      return 0
    case PROFILES_URL:
    case PROFILES_URL_ID:
      return 1
  }
  return 0
}

export const Clamp = (value: number, min: number, max: number) => {
  return Math.min(Math.max(value, min), max)
}

export function UseForceUpdate() {
  const [value, setValue] = useState(0); // integer state
  return () => setValue(value => ++value); // update the state to force render
}

export function GetCurrentMainComponent(pathName: string): string {
  console.log(pathName)
  switch (pathName) {
    case PROFILES_URL:
      return PROFILE_COMPONENTS
    case CHAT_VIEW_URL_ID:
      return CHAT_VIEW_COMPONENTS
  }

  return MISSION_COMPONENTS
}


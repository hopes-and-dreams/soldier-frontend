import {authState} from "../store/auth-store"

// Commons
export const REMOTE: string = "Remote";
export const PORT_REST: string = "8880"
export const PORT_WS: string = "8881"
export const SERVER_HOME: string = "localhost"

// Enum
export const ALL_TASK: string = "All Tasks";
export const COMPLETED: string = "Completed";
export const ON_GOING: string = "On Going";
export const HIRING: string = "Hiring";

// Auth
export const NOT_AUTHORIZED: authState = "NOT_AUTHORIZED"
export const AUTHENTICATING: authState = "AUTHENTICATING"
export const AUTHORIZED: authState = "AUTHORIZED"
export const JWT_COOKIE_NAME: string = "soldier_tag"
export const MOCKED_JWT: string = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0IiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.9NR-zIW7rrYVGk0KjLd4LKd7482VesXnZiT_XGZzgmU"

// Time
export const TWO_MINUTES: number = 120;
export const ONE_DAY: number = 18000;

// Event
export const SCROLL_UP: string = "scroll_up"
export const SCROLL_DOWN: string = "scroll_down"
export const HIRE_STATE: string = "Hire"
export const HIRED_STATE: string = "Hired"

// Styling
export const MAX_HEADER_HEIGHT: number = 50;

// ClassNames
export const MAIN_COMPONENT_WRAPPER: string = "mainComponentWrapper";
export const LIGHT_THEME: string = "light";
export const ROOT_ID: string = "#root";
export const ROOT: string = "root";
export const MISSION_COMPONENTS: string = "missionComponents";
export const PROFILE_COMPONENTS: string = "profileComponent";
export const CHAT_VIEW_COMPONENTS: string = "chatViewComponent";
// ID
export const ID_CAPTION: string = "caption";
export const ID_ADD_PHOTO: string = "add-photo";


// Url
export const NEWS_URL: string = "/news"
export const NEWS_URL_ID: string = "/news/:id"
export const MISSIONS_URL: string = "/missions"
export const MISSIONS_URL_ID: string = "/missions/:id"
export const PROFILES_URL: string = "/profile/[profileId]"
export const PROFILES_URL_ID: string = "/profile/:id"
export const CHAT_VIEW_URL_ID: string = "/chat/[receiverId]"
export const PROFILES_URL_ID_REVIEWS: string = PROFILES_URL_ID + "/reviews"
export const LOGIN_URL: string = "/login"
export const ACCOUNT_SETTING_URL: string = "/account-settings"

// Colors
export const COLOR_PRI_BLACK: string = "#9a9a9a"
export const COLOR_SEC_BLACK: string = "#424242"
export const COLOR_COPIED_GREEN: string = "#4BB543"
export const COLOR_WHITE: string = "#FFFFFF"

// Icon Sizes
export const SIZE_CHAIN: string = "3.5vw"
export const SIZE_SHARE_ICON: string = "4vw"

// Spring configs
export const SPR_CONF_HOR_SWIPE: object = {
  mass: 1,
  tension: 200,
  friction: 25,
}
export const SPR_CONF_RESPONSIVE: object = {
  mass: 1,
  tension: 400,
  friction: 25,
}

// Spring object constants
export const BREAK_PHOTO_VIEW_DISTANCE: number = 200

// Components name
export const PHOTO_VIEW = "photo-view"
export const PHOTO_ADD = "photo-add"